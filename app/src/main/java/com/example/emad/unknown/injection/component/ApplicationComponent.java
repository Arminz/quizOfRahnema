package com.example.emad.unknown.injection.component;

import android.app.Application;
import android.content.Context;

import com.example.emad.unknown.UnknownApplication;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.receiver.InstantiateMatchReceiver;
import com.example.emad.unknown.data.services.AlarmManagersHandler;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by emad on 4/6/17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(UnknownApplication app);

    void inject(AlarmManagersHandler servicesHandler);

    void inject(InstantiateMatchReceiver instantiateMatchReceiver);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

}
