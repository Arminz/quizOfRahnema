package com.example.emad.unknown.ui.secondSpecialQuiz;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.ui.base.MvpView;
import com.example.emad.unknown.ui.common.QuestionViewInterface;
import com.example.emad.unknown.ui.common.ResultViewInterface;
import com.example.emad.unknown.util.MatchSituation;

/**
 * Created by emad on 4/30/17.
 */

public interface SecondQuizMvpView extends MvpView, QuestionViewInterface, ResultViewInterface {

    void refreshTimeView(int sec, int max);

    void refreshQuestionView(Question question, int number,boolean hasGem);

    void showResult(MatchSituation matchSituation);

    void increaseXp(int deltaXp);

    void increaseGem(int deltaGem);

    void increaseCoin(int deltaCoin);

    void refreshPhaseView(int correct, int max, int phase, boolean upgrade);
}
