package com.example.emad.unknown.injection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by emad on 5/9/17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PreReceiver {
}
