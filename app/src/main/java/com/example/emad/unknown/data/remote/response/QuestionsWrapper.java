package com.example.emad.unknown.data.remote.response;

import com.example.emad.unknown.data.model.QuestionModel;

import java.util.ArrayList;

/**
 * Created by armin on 5/2/17.
 */

public class QuestionsWrapper {

    private ArrayList<QuestionModel> questions;
    private ArrayList<Integer> deleted;

    public QuestionsWrapper() {}

    public QuestionsWrapper(ArrayList<QuestionModel> questions, ArrayList<Integer> deleted) {
        this.questions = questions;
        this.deleted = deleted;
    }

    public ArrayList<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionModel> questions) {
        this.questions = questions;
    }

    public ArrayList<Integer> getDeleted() {
        return deleted;
    }

    public void setDeleted(ArrayList<Integer> deleted) {
        this.deleted = deleted;
    }
}
