package com.example.emad.unknown.injection.module;

import android.content.BroadcastReceiver;

import dagger.Module;

/**
 * Created by emad on 5/9/17.
 */

@Module
public class ReceiverModule {

    private final BroadcastReceiver mBroadcastReceiver;

    public ReceiverModule(BroadcastReceiver broadcastReceiver) {
        mBroadcastReceiver = broadcastReceiver;
    }

}
