package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.google.gson.Gson;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by armin on 5/7/17.
 */
@Table(database = DataBase.class)
public class MatchInstance extends BaseModel{

    @Column
    @PrimaryKey(autoincrement = true)
    private long id;

    @Column
    private int type;

    @Column
    private boolean seen;

    @Column
    private int maximumTime;

    @Column
    private int decreasingTime;

    @Column
    private int numberOfQuestion;

    @Column
    private int gemReward;

    @Column
    private int coinReward;

    @Column
    private int xpReward;

    @Column
    private int enterCoin;

    @Column
    private boolean consumed;

    public MatchInstance() {}

    public MatchInstance(Match match) {

//        this.seen = false;
        this.type = match.getType();
        this.maximumTime = match.getMaximumTime();
        this.coinReward = match.getCoinReward();
        this.xpReward = match.getXpReward();
        this.enterCoin = match.getEnterCoin();
        this.numberOfQuestion = match.getNumberOfQuestion();
        this.gemReward = match.getGemReward();
        this.consumed = false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMaximumTime() {
        return maximumTime;
    }

    public void setMaximumTime(int maximumTime) {
        this.maximumTime = maximumTime;
    }

    public int getDecreasingTime() {
        return decreasingTime;
    }

    public void setDecreasingTime(int decreasingTime) {
        this.decreasingTime = decreasingTime;
    }

    public int getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public void setNumberOfQuestion(int numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

    public int getGemReward() {
        return gemReward;
    }

    public void setGemReward(int gemReward) {
        this.gemReward = gemReward;
    }

    public int getCoinReward() {
        return coinReward;
    }

    public void setCoinReward(int coinReward) {
        this.coinReward = coinReward;
    }

    public int getXpReward() {
        return xpReward;
    }

    public void setXpReward(int xpReward) {
        this.xpReward = xpReward;
    }

    public int getEnterCoin() {
        return enterCoin;
    }

    public void setEnterCoin(int enterCoin) {
        this.enterCoin = enterCoin;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }


    public boolean isConsumed() {
        return consumed;
    }

    public void setConsumed(boolean consumed) {
        this.consumed = consumed;
    }
}
