package com.example.emad.unknown.data.manager;

import com.example.emad.unknown.data.local.entities.Answer;
import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.local.prefs.PreferencesHelper;
import com.example.emad.unknown.data.manager.remote.APIHelperInterface;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by emad on 4/6/17.
 */

public interface DataManager extends PreferencesHelper, APIHelperInterface {

//    Observable<Boolean> writeQuestionLocalObservable(Question question, List<Answer> answers);

    Observable<Question> readNewQuestionObservable();

    Observable<List<Question>> getQuestionsListForMatch(int listSize);

    Observable<UserDeltaDetails> getUserDeltaDetails();

    Observable<UserDetails> readUserDetailsObservable();

    Observable<Boolean> writeUserDetailsObservable(UserDetails userDetails);

    Observable<Boolean> writeMatchLocalObservable(Match match);

    Observable<Match> readMatchesLocalObservable();

    Observable<Boolean> writeQuestionLocalObservable(List<QuestionModel> questionModels);

    Observable<Boolean> writeMatchesInstancesLocalObservable(MatchInstance matchInstance);

    Observable<List<MatchInstance>> readMatchInstancesLocalObservable();

    Observable<Boolean> refreshDeltaDetails(int adCoins, int adGems, int adXp, int adLvl);

    MatchInstance readMatchInstance(long quizId);

    QuestionsSituationWrapper getQuestionsSituation();

    void setUserAuth(AuthResponse authResponse);

    void deleteSeenQuestions();

    long countUnseenQuestions();

    long getUserCoin();

    void consumeExtraGame(long id);

    void removeConsumedMatchInstances();

    boolean hasGem();
}
