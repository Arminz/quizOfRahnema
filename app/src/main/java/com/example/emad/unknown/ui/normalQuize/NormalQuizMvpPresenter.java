package com.example.emad.unknown.ui.normalQuize;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/11/17.
 */

public interface NormalQuizMvpPresenter<V extends NormalQuizMvpView> extends MvpPresenter<V> {

    void questionAnswered(Question question, int userAnswer);

    void getNewQuestion();

    void startQuiz();

    void onUseGem();

    void finishQuiz();

}
