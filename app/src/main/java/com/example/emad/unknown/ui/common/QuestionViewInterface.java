package com.example.emad.unknown.ui.common;

import com.example.emad.unknown.data.local.entities.Question;

/**
 * Created by emad on 4/12/17.
 */

public interface QuestionViewInterface {

    void onQuestionAnswered(Question question, int answer);

    void onUseGem();

}
