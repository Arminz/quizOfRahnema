package com.example.emad.unknown.ui.register;

import com.example.emad.unknown.injection.PerActivity;
import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 5/3/17.
 */

@PerActivity
public interface RegisterMvpPresenter<V extends RegisterMvpView> extends MvpPresenter<V> {

    boolean isSignedUp();

}
