package com.example.emad.unknown.data.model;

/**
 * Created by emad on 5/6/17.
 */

public class OptionsWrapperModel {

    private long id;

    private String text;

    private String numberOfHit;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNumberOfHit() {
        return numberOfHit;
    }

    public void setNumberOfHit(String numberOfHit) {
        this.numberOfHit = numberOfHit;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
