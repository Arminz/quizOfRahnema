package com.example.emad.unknown.ui.matchesInstancesList;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.emad.unknown.data.local.entities.MatchInstance;

import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/17/17.
 */

public class MatchesInstancesListPresenter extends BasePresenter<MatchesInstancesListMvpView>
        implements MatchesInstancesListMvpPresenter {

    @Inject
    public MatchesInstancesListPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void updateMatchInstancesList() {

//        final ArrayList<MatchInstance> matchesInstances = new ArrayList<>();

        getDataManager().readMatchInstancesLocalObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<MatchInstance>>() {
            @Override
            public void onCompleted() {

                Log.d(AppConstants.Log.ALGORITHM_FLOW,"updateMatchInstancesList -> onCompleted : ");

                getMvpView().refreshEnd();

//                getMvpView().setMatchesList(matchesInstances);

            }

            @Override
            public void onError(Throwable e) {

                Log.d(AppConstants.Log.ALGORITHM_FLOW,"updateMatchInstancesList -> onError : " + e.getMessage());

                getMvpView().refreshEnd();

            }

            @Override
            public void onNext(List<MatchInstance> matchInstances) {

                Log.d(AppConstants.Log.ALGORITHM_FLOW,"updateMatchInstancesList -> onNext : " + matchInstances.toString());

//                matchesInstances.add(matchInstance);
                getMvpView().setMatchesList((ArrayList<MatchInstance>) matchInstances);

            }
        });

    }

    @Override
    public void tryToStartQuiz(final long id, final int quizType) {

        final MatchInstance matchInstance = getDataManager().readMatchInstance(id);
        long userCoin = getDataManager().getUserCoin();
        if (userCoin < matchInstance.getEnterCoin())
        {
            Log.d(AppConstants.Log.ALGORITHM_FLOW,"not enough gold to start game!");
            getMvpView().notEnoughGold();

        }
        else if (matchInstance.getNumberOfQuestion() > getDataManager().countUnseenQuestions())
        {
            if (NetworkUtil.isNetworkConnected(getContext())) {
                getMvpView().showLoading();
                UserAuth userAuth = new UserAuth();
                userAuth.setAndroidId(Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                userAuth.setUsername(getDataManager().getUsername());
                userAuth.setPassword(getDataManager().getPassword());
                userAuth.setRefreshToken(getDataManager().getRefreshToken());
                userAuth.setAccessToken(getDataManager().getAccessToken());

                getDataManager().syncQuestionsRemoteObservable(getDataManager().getQuestionsSituation(),userAuth).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<QuestionsWrapper>() {
                            @Override
                            public void onCompleted() {
                                Log.d(AppConstants.Log.RETROFIT, "getQuestions onCompleted register");
                                getDataManager().deleteSeenQuestions();
                                getMvpView().hideLoading();

                            }

                            @Override
                            public void onError(Throwable e) {
                                getMvpView().hideLoading();
                                e.printStackTrace();
                                Log.d(AppConstants.Log.RETROFIT, "getQuestions onError : " + e.toString());
                                Toast.makeText(getContext(),"امکان بارگزاری بازی جدید به دلیل نبود سوال وجود ندارد.",Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onNext(QuestionsWrapper questionsWrapper) {
                                Gson gson = new Gson();
                                String json = gson.toJson(questionsWrapper);
                                Log.d(AppConstants.Log.RETROFIT, "getQuestions onNext : " + json);
                                getDataManager().writeQuestionLocalObservable(questionsWrapper.getQuestions()).subscribeOn(Schedulers.io()).subscribe(new Observer<Boolean>() {
                                    @Override
                                    public void onCompleted() {
                                        Log.d(AppConstants.Log.DBFlow, "write new questions complete");
                                        getDataManager().consumeExtraGame(id);
                                        startExtraGame(matchInstance,id,quizType);
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d(AppConstants.Log.DBFlow, "write new questions error : " + e.getMessage());
                                        Toast.makeText(getContext(),"امکان بارگزاری بازی جدید به دلیل نبود سوال وجود ندارد.",Toast.LENGTH_SHORT).show();

                                    }

                                    @Override
                                    public void onNext(Boolean aBoolean) {
                                        Log.d(AppConstants.Log.DBFlow, "write new questions onNext:" + aBoolean);

                                    }
                                });

                            }
                        });
            }
            else {
                Toast.makeText(getContext(),"اتصال اینترنت خود را برای دریافت سوال جدید بررسی کنید.",Toast.LENGTH_SHORT).show();
            }

        }
        else {
            getDataManager().consumeExtraGame(id);
            startExtraGame(matchInstance,id,quizType);
        }

    }
    private void startExtraGame(MatchInstance matchInstance,final long id,final int quizType)
    {
        getDataManager().refreshDeltaDetails(-1 * matchInstance.getEnterCoin(),0,0,0)
                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
        getMvpView().startExtraGame(id,quizType);
    }







}