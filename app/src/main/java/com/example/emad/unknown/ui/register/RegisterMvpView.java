package com.example.emad.unknown.ui.register;

import com.example.emad.unknown.ui.base.MvpView;

/**
 * Created by emad on 5/3/17.
 */

public interface RegisterMvpView extends MvpView {

    void navigateToMain();

    void navigateToSignIn();

    void navigateToSignUp();

}
