package com.example.emad.unknown.data.manager.remote.sockets;

import android.util.Log;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.APIError;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.ErrorUtils;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.util.AppConstants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by armin on 5/6/17.
 */

public class MatchesRemoteSocket implements Observable.OnSubscribe<Match> {
    private UserAuth userAuth;

    public static Observable<Match> getObservable(UserAuth userAuth){return Observable.create(new MatchesRemoteSocket(userAuth));}

    public MatchesRemoteSocket(UserAuth userAuth) {
        this.userAuth = userAuth;
    }

    @Override
    public void call(final Subscriber<? super Match> subscriber) {
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<ArrayList<Match>> arrayListCall = api.getMatches();
        arrayListCall.enqueue(new Callback<ArrayList<Match>>() {
            @Override
            public void onResponse(Call<ArrayList<Match>> call, Response<ArrayList<Match>> response) {
                if (response.isSuccessful()){
                    Log.d(AppConstants.Log.RETROFIT,response.raw().toString());
                    ArrayList<Match> matches = response.body();
                    for (int i = 0 ; i < matches.size() ; i ++)
                        subscriber.onNext(matches.get(i));
                    subscriber.onCompleted();
                }
                else
                {
                    APIError error = ErrorUtils.parseError(response);
                    if (error.message() != null) {
                        Log.d(AppConstants.Log.RETROFIT, error.message());
                        subscriber.onError(new Throwable(error.message()));
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Match>> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"error connecting to server...");
                subscriber.onError(t);

            }
        });

    }
}
