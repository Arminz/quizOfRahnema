package com.example.emad.unknown.injection.component;

import com.example.emad.unknown.injection.PerActivity;
import com.example.emad.unknown.injection.module.ActivityModule;
import com.example.emad.unknown.ui.firstSpecialQuiz.FirstQuizFragment;
import com.example.emad.unknown.ui.matchesInstancesList.MatchesInstancesListFragment;
import com.example.emad.unknown.ui.matchesList.MatchesListFragment;
import com.example.emad.unknown.ui.main.MainActivity;
import com.example.emad.unknown.ui.menu.MenuFragment;
import com.example.emad.unknown.ui.normalQuize.NormalQuizFragment;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.ui.register.RegisterActivity;
import com.example.emad.unknown.ui.secondSpecialQuiz.SecondQuizFragment;
import com.example.emad.unknown.ui.signIn.SignInFragment;
import com.example.emad.unknown.ui.signUp.SignUpFragment;

import dagger.Component;

/**
 * Created by emad on 4/6/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(NormalQuizFragment fragment);

    void inject(MenuFragment fragment);

    void inject(MatchesListFragment fragment);

    void inject(QuizActivity activity);

    void inject(FirstQuizFragment fragment);

    void inject(SecondQuizFragment fragment);

    void inject(RegisterActivity activity);

    void inject(SignInFragment fragment);

    void inject(SignUpFragment fragment);

    void inject(MatchesInstancesListFragment matchesInstancesListFragment);

    // inject activities presenters here
}
