package com.example.emad.unknown.ui.matchesInstancesList;

import android.view.View;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.model.GameTitle;
import com.example.emad.unknown.ui.base.MvpView;

import java.util.ArrayList;

/**
 * Created by emad on 4/17/17.
 */

public interface MatchesInstancesListMvpView extends MvpView {

    void requestExtraGame(long id, int quizType);

    void setMatchesList(ArrayList<MatchInstance> matchesInstances);

    void refreshEnd();

    void startExtraGame(long id, int quizType);

    void forbiddenMatchClicked(View view);

    void notEnoughGold();

}