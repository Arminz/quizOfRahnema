package com.example.emad.unknown.data.manager;

import android.util.Log;

import com.example.emad.unknown.data.local.entities.ExtraGame1;
import com.example.emad.unknown.data.local.entities.ExtraGame2;
import com.example.emad.unknown.data.model.GameTitle;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;


/**
 * Created by armin on 4/23/17.
 */

public class ExtraGamesListProvider {
    public static ArrayList<GameTitle> fetchGameTitles()
    {
        ArrayList<GameTitle> gameTitles = new ArrayList<>();
        gameTitles.addAll(gameTitleFromExtraGames1());
        gameTitles.addAll(gameTitleFromExtraGames2());

        return gameTitles;

    }
    public static ArrayList<GameTitle> gameTitleFromExtraGames1()
    {
        Observable<ExtraGame1> observable = ExtraGame1sObservable.getInstance();
        final ArrayList < GameTitle > gameTitles = new ArrayList<>();
        observable.subscribe(new Observer<ExtraGame1>() {
                                 @Override
                                 public void onNext(ExtraGame1 extraGame1) {
                                     gameTitles.add(new GameTitle(extraGame1));
                                 }

                                 @Override
                                 public void onError(Throwable e) {

                                 }

                                 @Override
                                 public void onCompleted() {
                                     Log.d("tag","done");

                                 }
                             });
        return gameTitles;
    }
    public static ArrayList<GameTitle> gameTitleFromExtraGames2()
    {
        List<ExtraGame2> extraGame2s = SQLite.select().from(ExtraGame2.class).queryList();
        ArrayList<GameTitle> gameTitles = new ArrayList<>();
        for (int i = 0 ; i < extraGame2s.size() ; i ++)
            gameTitles.add(new GameTitle(extraGame2s.get(i)));
        return gameTitles;
    }
}
