package com.example.emad.unknown.ui.secondSpecialQuiz;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.data.local.entities.Answer;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.HitResponse;
import com.example.emad.unknown.util.MatchSituation;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/30/17.
 */

public class SecondQuizPresenter<V extends SecondQuizMvpView> extends BasePresenter<V>
        implements SecondQuizMvpPresenter<V>{

    private int mSickTimer;

    Subscription subscription;

    private int maxTime;

    private MatchInstance matchInstance;


    private static MatchSituation matchSituation;

    @Inject
    public SecondQuizPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void startQuiz(long quizId) {

        matchInstance = getDataManager().readMatchInstance(quizId);

        matchSituation = new MatchSituation(matchInstance);

        maxTime = matchInstance.getMaximumTime();

        setTimer(maxTime);
        getMvpView().refreshTimeView(maxTime,maxTime);
        getQuestion();
        startTimer();
    }

    @Override
    public void getQuestion() {
        getMvpView().showLoading();
        if (matchInstance.getNumberOfQuestion() <= matchSituation.getPassedQuestions()) {
            finishQuiz();
            getMvpView().hideLoading();
            return;
        }

        getDataManager().readNewQuestionObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Question>() {
                    @Override
                    public void onCompleted() {
                        Log.d(AppConstants.Log.DBFlow,"readNewQuestionObservable -> onCompleted.");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(AppConstants.Log.DBFlow,"readNewQuestionObservable -> onError : " + e.getMessage());
                        Toast.makeText(getContext(),"سوالات تمام شده است.",Toast.LENGTH_SHORT).show();
                        getMvpView().hideLoading();
                        finishQuiz();
                    }

                    @Override
                    public void onNext(Question question) {
                        question.setDuplicate(true);
                        question.setUseDate(new Date());
                        question.update();
                        getMvpView().refreshQuestionView(question, matchSituation.getPassedQuestions(),getDataManager().hasGem());
                        getMvpView().hideLoading();
                    }
                });
    }

    @Override
    public void questionAnswered(Question question, int userAnswer) {
        final HitResponse hitResponse;
        if (question.getCorrectAnswer() == userAnswer)
        {
            hitResponse = matchSituation.hitCorrect();
//            getDataManager().getUserDeltaDetails()
//                    .observeOn(Schedulers.io())
//                    .subscribeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Action1<UserDeltaDetails>() {
//                        @Override
//                        public void call(UserDeltaDetails userDeltaDetails) {
//                            userDeltaDetails.incrementXp(hitResponse.getXpReward());
//                            userDeltaDetails.save();
//                        }
//                    });
//            addTimeToTimer(matchInstance.getDecreasingTime());
            addTimeToTimer(2);
            getMvpView().refreshTimeView(mSickTimer, maxTime);
            Log.d(AppConstants.Log.GAME_SITUATION,"HitResponse -> true : " + hitResponse.toString());


        }
        else
        {
            hitResponse = matchSituation.hitWrong();
            Log.d(AppConstants.Log.ALGORITHM_FLOW,"HitResponse -> false : " + hitResponse.toString());
        }
        getMvpView().refreshPhaseView(matchSituation.getCurrentPhaseHitCorrectAnswers(),matchSituation.getCurrentPhaseTotalCorrectAnswers(),matchSituation.getCurrentPhase(),hitResponse.isPhaseUpgraded());
        question.setUserAnswer(userAnswer);
        question.update();

        Answer hitAnswer = question.getQuestionAnswers().get(userAnswer);
        hitAnswer.setIsHit(true);
        hitAnswer.update();
    }


    @Override
    public void startTimer() {
        subscription = getCountDownTimer().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        mSickTimer --;
                        Log.d("timer", "timer: "+mSickTimer);
                        //getMvpView().refreshTimeView(mSickTimer, maxTime);
                        if (mSickTimer == 0) {
                            finishQuiz();
                            unSubscribe();
                        }
                    }
                });
    }

    @Override
    public void setTimer(int timeInSec) {
        mSickTimer = timeInSec;
    }

    @Override
    public void addTimeToTimer(int sec) {
        mSickTimer += sec;
        if (mSickTimer > maxTime){
            mSickTimer = maxTime;
        }
    }

    @Override
    public void finishQuiz() {

        unSubscribe();


        getDataManager().refreshDeltaDetails(matchSituation.getPhaseCoin() + matchSituation.getCoinOne(),
                matchSituation.getPhaseGem() + matchSituation.getGemOne(),
                matchSituation.getPhaseXp() + matchSituation.getXpOne()
                ,0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onCompleted" );

                        getMvpView().showResult(matchSituation);


                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onNext: " + e.getMessage());
//                        Toast.makeText(getContext(),"مشکلی در اضافه کردن جوایز به وجود آمده.",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onNext: " + aBoolean);

                    }
                });


    }

    @Override
    public void onUseGem() {
        getDataManager().refreshDeltaDetails(0,-1,0,0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public void unSubscribe(){
        if (!subscription.isUnsubscribed())
            subscription.unsubscribe();
    }

    public Observable<Long> getCountDownTimer(){
        return Observable.interval(1, TimeUnit.SECONDS, Schedulers.io());
    }

    @Override
    public void onDetach() {
        unSubscribe();
        super.onDetach();
    }
}
