package com.example.emad.unknown.ui.matchesList;

import android.content.Context;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/17/17.
 */

public class MatchesListPresenter extends BasePresenter<MatchesListMvpView>
        implements MatchesListMvpPresenter {

    @Inject
    public MatchesListPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void updateMatchesList() {

        final ArrayList<Match> matches = new ArrayList<>();

        getDataManager().readMatchesLocalObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Match>() {
            @Override
            public void onCompleted() {

                getMvpView().setMatchesList(matches);

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Match match) {

                matches.add(match);

            }
        });

    }
}