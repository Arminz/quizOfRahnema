package com.example.emad.unknown.ui.secondSpecialQuiz;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/30/17.
 */

public interface SecondQuizMvpPresenter<V extends SecondQuizMvpView> extends MvpPresenter<V> {

    void startQuiz(long quizId);

    void getQuestion();

    void questionAnswered(Question question, int userAnswer);

    void startTimer();

    void setTimer(int timeInSec);

    void addTimeToTimer(int sec);

    void finishQuiz();

    void onUseGem();

}
