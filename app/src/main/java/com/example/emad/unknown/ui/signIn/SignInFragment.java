package com.example.emad.unknown.ui.signIn;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.emad.unknown.R;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.register.RegisterActivity;
import com.example.emad.unknown.util.AppConstants;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by emad on 5/3/17.
 */

public class SignInFragment extends BaseFragment implements SignInMvpView {

    public static final String TAG = "SignInFragment";

    @BindView(R.id.sign_in_button)
    Button signInBtn;

    @BindView(R.id.link_to_sign_up_text_view)
    Button linkToSignUp;

    @BindView(R.id.pass_edit_text)
    EditText passwordET;

    @BindView(R.id.username_edit_text)
    EditText usernameET;

    @Inject
    SignInMvpPresenter<SignInMvpView> mPresenter;

    public static SignInFragment newInstance() {
        Bundle args = new Bundle();
        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.link_to_sign_up_text_view)
    void linkToSignUpFunc(){
        ((RegisterActivity)getActivity()).navigateToSignUp();
    }

    @OnClick(R.id.sign_in_button)
    void signInUser(){
        mPresenter.signIn(usernameET.getText().toString(), passwordET.getText().toString());
    }


    @Override
    public void navigateToMain() {
        ((RegisterActivity)getActivity()).navigateToMain();
    }

    @Override
    public void disableSignInBtn() {
        signInBtn.setEnabled(false);
    }

    @Override
    public void enableSignInBtn() {
        signInBtn.setEnabled(true);
    }

    @Override
    public void signInError(String type) {
        Log.d(AppConstants.Log.ALGORITHM_FLOW,"signInError -> type: " + type);
        switch (type){
            case AppConstants.SignErrors.usernameEmpty:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(usernameET);
                break;
            case AppConstants.SignErrors.passwordEmpty:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(passwordET);
                break;
            case AppConstants.SignErrors.NO_INTERNET_CONNECTION:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(signInBtn);
                Toast.makeText(getContext(),"اتصال اینترنت خود را بررسی کنید.",Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.SignErrors.ERROR_CONNECTING_SERVER:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(signInBtn);
                Toast.makeText(getContext(),"مشکل در ارتباط با سرور",Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.SignErrors.WRONG_PASSWORD_USERNAME:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(usernameET);
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(passwordET);
                Toast.makeText(getContext(),"نام کاربری یا پسورد غلط",Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getContext(),type,Toast.LENGTH_SHORT).show();




        }


    }
}
