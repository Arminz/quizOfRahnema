package com.example.emad.unknown.ui.menu;

import com.example.emad.unknown.ui.base.MvpView;

/**
 * Created by emad on 4/17/17.
 */

public interface MenuMvpView extends MvpView {

    void showNormalQuiz();

}
