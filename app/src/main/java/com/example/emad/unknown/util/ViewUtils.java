package com.example.emad.unknown.util;

import android.content.res.Resources;

/**
 * Created by emad on 4/11/17.
 */

public class ViewUtils {

    private ViewUtils() {}

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }
}
