package com.example.emad.unknown.ui.common;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.Answer;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.injection.ApplicationContext;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.LongClick;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

import javax.inject.Inject;

import tyrantgit.explosionfield.ExplosionField;


/**
 * Created by emad on 4/1/17.
 */

@NonReusable
@Layout(R.layout.question_layout)
public class QuestionView{


    private static final String TAG = "QuestionCard";

    @View(R.id.card_layout)
    private CardView mCardView;

    @View(R.id.tv_question_txt)
    private TextView mQuestionTextView;

    @View(R.id.btn_option_1)
    private Button mOption1Button;

    @View(R.id.btn_option_2)
    private Button mOption2Button;

    @View(R.id.use_gem)
    private ImageView gemBtn;

    @View(R.id.number_qus_textView)
    private TextView questionNumberTV;

    private int questionNumber;

    private Question mQuestion;

    private QuestionViewInterface mQuestionViewInterface;

    private Context context;

    private ExplosionField mExplosionField;

    private Activity activity;

    private boolean isExplode;

    private boolean hasGem;

    public QuestionView(Question question, QuestionViewInterface questionViewInterface, Context context, int questionNumber, Activity activity,boolean hasGem) {
        mQuestion = question;
        this.mQuestionViewInterface = questionViewInterface;
        this.context= context;
        this.questionNumber = questionNumber;
        this.activity = activity;
        isExplode = false;
        this.hasGem = hasGem;
    }


    @Resolve
    private void onResolved() {

        questionNumberTV.setText("سوال: "+ Integer.toString(questionNumber)
                .replaceAll("1","١").replaceAll("0","۰")
                .replaceAll("3","۳").replaceAll("2","۲")
                .replaceAll("5","۵").replaceAll("4","۴")
                .replaceAll("7","۷").replaceAll("6","۶")
                .replaceAll("9","۹").replaceAll("8","۸"));
        mQuestionTextView.setText(mQuestion.getQuestion());

        mExplosionField = ExplosionField.attach2Window(activity);

        for (int i = 0; i < 2; i++) {
            Button button = null;
            switch (i) {
                case 0:
                    button = mOption1Button;
                    break;
                case 1:
                    button = mOption2Button;
                    break;
            }

            if (button != null)
                button.setText(mQuestion.getQuestionAnswers().get(i).getAnswer());
        }
    }

    private void showCorrectOptions(int userAns) {

        for (int i = 0 ; i < 2 ; i++) {
            Button button = null;
            switch (i) {
                case 0:
                    button = mOption1Button;
                    break;
                case 1:
                    button = mOption2Button;
                    break;
            }
            if (button != null) {
                button.setEnabled(false);
            }
            if ((i == userAns)&& (i != mQuestion.getCorrectAnswer()) && (button != null)){
                button.setBackground(context.getResources().getDrawable(R.drawable.button_drawable_red));
            }
            if ((i == mQuestion.getCorrectAnswer()) && (button != null)){
                button.setBackground(context.getResources().getDrawable(R.drawable.button_drawable_green));
            }
        }

    }

    @Click(R.id.btn_option_1)
    @LongClick(R.id.btn_option_1)
    public void onOption1Click() {
        isExplode =true;
        mQuestionViewInterface.onQuestionAnswered(mQuestion, 0);
        showCorrectOptions(0);
        gemBtn.setEnabled(false);
    }

    @Click(R.id.btn_option_2)
    @LongClick(R.id.btn_option_2)
    public void onOption2Click() {
        isExplode =true;
        mQuestionViewInterface.onQuestionAnswered(mQuestion, 1);
        showCorrectOptions(1);
        gemBtn.setEnabled(false);
    }

    @Click(R.id.use_gem)
    @LongClick(R.id.use_gem)
    public void useGem() {
        if (hasGem) {
            mQuestionViewInterface.onUseGem();

            mQuestionViewInterface.onQuestionAnswered(mQuestion, mQuestion.getCorrectAnswer());
            //        mQuestionViewInterface.onQuestionAnswered(mQuestion, 1);

            if (mQuestion.getCorrectAnswer() != 0) {
                mExplosionField.explode(mOption1Button);
            }
            if (mQuestion.getCorrectAnswer() != 1) {
                mExplosionField.explode(mOption2Button);
            }
            gemBtn.setEnabled(false);
            showCorrectOptions(mQuestion.getCorrectAnswer());
        }
        else {
            YoYo.with(Techniques.Shake)
                    .duration(500)
                    .repeat(1)
                    .playOn(gemBtn);
        }

    }

}
