package com.example.emad.unknown.ui.signUp;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 5/3/17.
 */

public class SignUpPresenter<V extends SignUpMvpView> extends BasePresenter<V>
        implements SignUpMvpPresenter<V> {

    @Inject
    public SignUpPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void registerUser(final String username, final String password) {

        if (username.trim().isEmpty())
            getMvpView().signUpError(AppConstants.SignErrors.usernameEmpty);
        if (password.trim().isEmpty())
            getMvpView().signUpError(AppConstants.SignErrors.passwordEmpty);
        if (!NetworkUtil.isNetworkConnected(getContext()))
            getMvpView().signUpError(AppConstants.SignErrors.NO_INTERNET_CONNECTION);
        if (username.trim().isEmpty() || password.trim().isEmpty())
            return;


        if (NetworkUtil.isNetworkConnected(getContext())) {
            getMvpView().showProgressBar();
            getMvpView().disableSignUpBtn();

            UserAuth userAuth = new UserAuth();

            userAuth.setAndroidId(Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
            userAuth.setUsername(username);
            userAuth.setPassword(password);
            userAuth.setType(AppConstants.GRANT_TYPE.PASSWORD);

            getDataManager().registerUserObservable(userAuth).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<UserDetails>() {
                        @Override
                        public void onCompleted() {
                            Log.d(AppConstants.Log.RETROFIT,"signUp -> onCompleted");
                            getMvpView().hideProgressBar();
                            getMvpView().enableSignUpBtn();
                            getDataManager().setSignedUp(true);
                            getMvpView().navigateToMain();
                        }
                        @Override
                        public void onError(Throwable e) {
                            Log.d(AppConstants.Log.RETROFIT,"signUp -> onError : " + e.toString());
                            getMvpView().hideProgressBar();
                            getMvpView().enableSignUpBtn();
                            e.printStackTrace();
                            getMvpView().signUpError(e.getMessage());

                        }
                        @Override
                        public void onNext(UserDetails userDetails) {
                            Gson gson = new Gson();
                            String json = gson.toJson(userDetails);
                            Toast.makeText(getContext(),"onNext : " + json , Toast.LENGTH_SHORT).show();
                            Log.d(AppConstants.Log.RETROFIT,"signUp -> onNext : " + json);
                            getDataManager().writeUserDetailsObservable(userDetails);
                            getDataManager().setUsername(username);
                            getDataManager().setPassword(password);

                        }
                    });
        }else {
            getMvpView().onError("please connect to the Internet.");
        }
    }
}
