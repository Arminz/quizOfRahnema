package com.example.emad.unknown.ui.common;

import android.widget.TextView;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.ResultData;
import com.example.emad.unknown.util.ConvertToArabicNumber;
import com.example.emad.unknown.util.MatchSituation;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.LongClick;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;


/**
 * Created by emad on 5/25/17.
 */

@NonReusable
@Layout(R.layout.result_layout)
public class ResultView{


    private static final String TAG = "ResultCard";

    @View(R.id.per_q_coin)
    private TextView coinPerQTV;

    @View(R.id.reward_coin)
    private TextView coinRewardTV;

    @View(R.id.per_q_gem)
    private TextView gemPerQTV;

    @View(R.id.reward_gem)
    private TextView gemRewardTV;

    @View(R.id.per_q_xp)
    private TextView xpPerQTV;

    @View(R.id.reward_xp)
    private TextView xpRewardTV;

    @View(R.id.total_coin)
    private TextView totalCoin;

    @View(R.id.total_gem)
    private TextView totalGem;

    @View(R.id.total_xp)
    private TextView totalXp;

    @View(R.id.cardType)
    private TextView typeTV;

    ResultViewInterface mResultViewInterface;

    MatchSituation mResultData;

    String type;


    public ResultView(MatchSituation matchSituation, ResultViewInterface resultViewInterface, String type) {
        mResultViewInterface = resultViewInterface;
        mResultData = matchSituation;
        this.type = type;
    }


    @Resolve
    private void onResolved() {
        typeTV.setText(type);
        xpPerQTV.setText(ConvertToArabicNumber.convert(""+mResultData.getXpOne()));
        xpRewardTV.setText(ConvertToArabicNumber.convert(""+mResultData.getPhaseXp()));
        coinPerQTV.setText(ConvertToArabicNumber.convert(""+mResultData.getCoinOne()));
        coinRewardTV.setText(ConvertToArabicNumber.convert(""+mResultData.getPhaseCoin()));
        gemPerQTV.setText(ConvertToArabicNumber.convert(""+mResultData.getGemOne()));
        gemRewardTV.setText(ConvertToArabicNumber.convert(""+mResultData.getPhaseGem()));
        totalCoin.setText(ConvertToArabicNumber.convert(""+(mResultData.getCoinOne()+mResultData.getPhaseCoin())));
        totalGem.setText(ConvertToArabicNumber.convert(""+(mResultData.getGemOne()+mResultData.getPhaseGem())));
        totalXp.setText(ConvertToArabicNumber.convert(""+(mResultData.getXpOne()+mResultData.getPhaseXp())));
    }


    @Click(R.id.close_btn)
    @LongClick(R.id.close_btn)
    public void onOption1Click() {
        mResultViewInterface.onCloseClick();
    }


}
