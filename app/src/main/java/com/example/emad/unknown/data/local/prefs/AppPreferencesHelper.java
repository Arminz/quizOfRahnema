package com.example.emad.unknown.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.injection.PreferenceInfo;


import java.sql.Timestamp;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by emad on 4/6/17.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_SIGNED_UP = "PREF_KEY_SIGNED_UP";
    private static final String PREF_KEY_LAST_SYNC_DETAILS = "PREF_KEY_LAST_SYNC_DETAILS";
    private static final String PREF_KEY_LAST_SYNC_MATCHES= "PREF_KEY_LAST_SYNC_MATCHES";
    private static final String PREF_KEY_LAST_SYNC_QUESTIONS = "PREF_KEY_LAST_SYNC_QUESTIONS";

    private static final String PREF_KEY_USERNAME = "PREF_KEY_USERNAME";
    private static final String PREF_KEY_PASSWORD = "PREF_KEY_PASSWORD";
    private static final String PREF_KEY_REFRESH_TOKEN= "PREF_KEY_REFRESH_TOKEN";
    private static final String PREF_KEY_ACCESS_TOKEN= "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_TOKEN_TYPE= "PREF_KEY_TOKEN_TYPE";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void setSignedUp(boolean situation) {
        mPrefs.edit().putBoolean(PREF_KEY_SIGNED_UP, situation).apply();
    }

    @Override
    public boolean isSignedUp() {
        return mPrefs.getBoolean(PREF_KEY_SIGNED_UP, false);
    }

    @Override
    public Timestamp getUserLastSyncDetails() {
        return new Timestamp(mPrefs.getLong(PREF_KEY_LAST_SYNC_DETAILS,0));
    }

    @Override
    public void setUserLastSyncDetails(Timestamp syncTime) {
        mPrefs.edit().putLong(PREF_KEY_LAST_SYNC_DETAILS, syncTime.getTime()).apply();
    }

    @Override
    public Timestamp getUserLastSyncMatches() {
        return new Timestamp(mPrefs.getLong(PREF_KEY_LAST_SYNC_MATCHES,0));
    }

    @Override
    public void setUserLastSyncMatches(Timestamp syncTime) {
        mPrefs.edit().putLong(PREF_KEY_LAST_SYNC_MATCHES,syncTime.getTime()).apply();
    }

    @Override
    public Timestamp getUserLastSyncQuestions() {
        return new Timestamp(mPrefs.getLong(PREF_KEY_LAST_SYNC_QUESTIONS,0));
    }

    @Override
    public void setUserLastSyncQuestions(Timestamp syncTime) {
        mPrefs.edit().putLong(PREF_KEY_LAST_SYNC_QUESTIONS,syncTime.getTime()).apply();
    }

    @Override
    public String getUsername() {
        return mPrefs.getString(PREF_KEY_USERNAME,"");
    }

    @Override
    public void setUsername(String username) {
        mPrefs.edit().putString(PREF_KEY_USERNAME,username).apply();
    }

    @Override
    public String getPassword() {
        return mPrefs.getString(PREF_KEY_PASSWORD,"");
    }

    @Override
    public void setPassword(String password) {
        mPrefs.edit().putString(PREF_KEY_PASSWORD,password).apply();
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN,"");
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN,accessToken).apply();
    }

    @Override
    public String getRefreshToken() {
        return mPrefs.getString(PREF_KEY_REFRESH_TOKEN,"");
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        mPrefs.edit().putString(PREF_KEY_REFRESH_TOKEN,refreshToken).apply();
    }

    @Override
    public String getTokenType() {
        return mPrefs.getString(PREF_KEY_TOKEN_TYPE,"");
    }

    @Override
    public void setTokenType(String tokenType) {
        mPrefs.edit().putString(PREF_KEY_TOKEN_TYPE,tokenType).apply();
    }

}
