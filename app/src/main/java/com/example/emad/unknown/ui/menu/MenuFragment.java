package com.example.emad.unknown.ui.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.emad.unknown.R;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.main.MainActivity;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.DialogFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by emad on 4/17/17.
 */

public class MenuFragment extends BaseFragment implements MenuMvpView {

    public static final String TAG = "MenuFragment";

    @BindView(R.id.normal_quiz_btn)
    Button normalQuizBtn;


    @Inject
    MenuMvpPresenter<MenuMvpView> mPresenter;

    public static MenuFragment newInstance() {
        Bundle args = new Bundle();
        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void showNormalQuiz() {
        ((MainActivity)getActivity()).navigateToQuizActivity(AppConstants.NORMAL_QUIZ, 0);
    }


    @OnClick(R.id.normal_quiz_btn)
    void showQuiz(){
//        DialogFactory.showAboutDialog(getContext());
        mPresenter.tryToStartNormalQuiz();
    }


//    @OnClick(R.id.matches_instances_list_btn)
//    void showMatchesInstances(){
//        showMatchesInstancesList();
//    }

}
