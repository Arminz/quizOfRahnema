package com.example.emad.unknown.util;

import com.google.gson.Gson;

/**
 * Created by armin on 5/23/17.
 */

public class HitResponse {
    private boolean isPhaseUpgraded = false;
    private int xpReward = 0;

    private int phaseXpReward = 0;
    private int phaseGemReward = 0;
    private int phaseCoinReward = 0;


    public boolean isPhaseUpgraded() {
        return isPhaseUpgraded;
    }

    public void setPhaseUpgraded(boolean phaseUpgraded) {
        isPhaseUpgraded = phaseUpgraded;
    }

    public int getXpReward() {
        return xpReward;
    }

    public void setXpReward(int xpReward) {
        this.xpReward = xpReward;
    }

    public int getPhaseXpReward() {
        return phaseXpReward;
    }

    public void setPhaseXpReward(int phaseXpReward) {
        this.phaseXpReward = phaseXpReward;
    }

    public int getPhaseGemReward() {
        return phaseGemReward;
    }

    public void setPhaseGemReward(int phaseGemReward) {
        this.phaseGemReward = phaseGemReward;
    }

    public int getPhaseCoinReward() {
        return phaseCoinReward;
    }

    public void setPhaseCoinReward(int phaseCoinReward) {
        this.phaseCoinReward = phaseCoinReward;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
