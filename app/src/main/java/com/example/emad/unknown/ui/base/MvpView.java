package com.example.emad.unknown.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by emad on 4/4/17.
 */

public interface MvpView {

    void showLoading();

    void hideLoading();

    void onError(@StringRes int resId);

    void onError(String message);

}
