package com.example.emad.unknown.ui.normalQuize;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.data.local.entities.Answer;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.MatchSituation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/11/17.
 */

public class NormalQuizPresenter<V extends NormalQuizMvpView> extends BasePresenter<V>
        implements NormalQuizMvpPresenter<V>{

    private int questionsPassed;

    private int correctAnswers;

    @Inject
    public NormalQuizPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void questionAnswered(Question question, int userAnswer) {

        if (question.getCorrectAnswer() == userAnswer){
            correctAnswers ++;
        }else {
            getMvpView().explodeHeart();
        }

        question.setUserAnswer(userAnswer);
        question.update();

        Answer hitAnswer = question.getQuestionAnswers().get(userAnswer);
        hitAnswer.setIsHit(true);
        hitAnswer.update();

    }

    @Override
    public void getNewQuestion() {
        getMvpView().showLoading();
        questionsPassed ++;
        if (questionsPassed > 20){
            finishQuiz();
            return;
        }
//        Question question = new Question();
//        question.setDuplicate(false);
//        question.setQuestion("2+2 = ?");
//        question.setCorrectAnswer(1);
//        List<Answer> answers = new ArrayList<>();
//        Answer answer = new Answer();
//        answer.setAnswer("3");
//        answers.add(answer);
//        answer = new Answer();
//        answer.setAnswer("4");
//        answers.add(answer);
//        answer = new Answer();
//        answer.setAnswer("5");
//        answers.add(answer);
//        answer = new Answer();
//        answer.setAnswer("6");
//        answers.add(answer);
//        getDataManager().writeQuestionLocalObservable(question,answers)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<Boolean>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onNext(Boolean aBoolean) {
//
//                    }
//                });
        getDataManager().readNewQuestionObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Question>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("data_","::"+e.getMessage());
                        getMvpView().onError("سوالات تمام شده است.");
                        getMvpView().hideLoading();
                        finishQuiz();
                    }

                    @Override
                    public void onNext(Question question) {
                        question.setUseDate(new Date());
                        question.setDuplicate(true);
                        question.update();
                        getMvpView().refreshQuestionView(question, questionsPassed,getDataManager().hasGem());
                        getMvpView().hideLoading();
                    }
                });
    }

    @Override
    public void startQuiz() {
        getNewQuestion();
        getMvpView().refreshUserDetailView();
        correctAnswers ++;
        questionsPassed = 1;
    }

    @Override
    public void onUseGem() {
        getDataManager().refreshDeltaDetails(0,-1,0,0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    @Override
    public void finishQuiz() {
        final MatchSituation matchSituation;
        final boolean victory;
        if (correctAnswers >= 17) {
            victory = true;
            matchSituation = new MatchSituation(300);
        }
        else {
            victory = false;
            matchSituation = new MatchSituation(0);
        }

        getDataManager().refreshDeltaDetails(matchSituation.getCoinOne() + matchSituation.getPhaseCoin(),
                matchSituation.getGemOne() + matchSituation.getPhaseGem(),
                matchSituation.getXpOne() + matchSituation.getPhaseXp(),0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onCompleted" );
                        getMvpView().showResult(matchSituation,victory);
                        getMvpView().hideLoading();

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onNext: " + e.getMessage());

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onNext: " + aBoolean);

                    }
                });

//        getMvpView().showResult(matchSituation,victory);
    }

}
