package com.example.emad.unknown.ui.matchesList;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 4/17/17.
 */

public class MatchesListAdapter extends RecyclerView.Adapter<MatchesListAdapter.ViewHolder> {
    private List<Match> matches;
    private MatchesListMvpView gamesListMvpView;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView infoTextView;
        public ViewHolder(final View view) {
            super(view);
            CardView cardView = (CardView) view.findViewById(R.id.card_view);
            this.infoTextView = (TextView) cardView.findViewById(R.id.info_textView);

        }
    }


    public MatchesListAdapter(ArrayList<Match> matches, MatchesListMvpView gamesListMvpView) {
        this.matches = matches;
        this.gamesListMvpView = gamesListMvpView;
    }

    public void setMatches(ArrayList<Match> matches){
        this.matches = matches;
    }

    @Override
    public MatchesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_row,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override

    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.infoTextView.setText((int) matches.get(position).getId() + " : " + matches.get(position).toString());
//        holder.infoTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(holder.getAdapterPosition()%2 == 0)
//                    gamesListMvpView.requestExtraGame(matches.get(holder.getAdapterPosition()).getId(), AppConstants.QUIZ_TYPE_ONE);
//                else
//                    gamesListMvpView.requestExtraGame(matches.get(holder.getAdapterPosition()).getId(), AppConstants.QUIZ_TYPE_TWO);
//
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return matches.size();
    }
}

