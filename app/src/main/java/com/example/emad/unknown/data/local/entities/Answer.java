package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.OptionsWrapperModel;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by emad on 3/15/17.
 */

@Table(database = DataBase.class)
public class Answer extends BaseModel {

    @Column
    long id ;

    @Column
    @PrimaryKey(autoincrement = true)
    long localId;

    @Column
    String answer ;

    @ForeignKey(stubbedRelationship = true)
    Question question ;

    @Column(defaultValue = "false")
    boolean isHit;

    @Column
    int numberOfHit;

    public Answer(){}

    public Answer(OptionsWrapperModel optionsWrapperModel, Question question) {
        this.answer = optionsWrapperModel.getText();
        this.question = question;
        this.isHit = false;
        this.id = optionsWrapperModel.getId();
//        this.numberOfHit = Integer.parseInt(optionsWrapperModel.getNumberOfHit());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public boolean getIsHit() {
        return isHit;
    }

    public void setIsHit(boolean isHit) {
        this.isHit = isHit;
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public boolean isHit() {
        return isHit;
    }

    public void setHit(boolean hit) {
        isHit = hit;
    }

    public int getNumberOfHit() {
        return numberOfHit;
    }

    public void setNumberOfHit(int numberOfHit) {
        this.numberOfHit = numberOfHit;
    }
}
