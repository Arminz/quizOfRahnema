package com.example.emad.unknown.data.manager;

import android.content.Context;
import android.util.Log;

import com.example.emad.unknown.data.local.entities.Answer;
import com.example.emad.unknown.data.local.entities.Answer_Table;
import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.MatchInstance_Table;
import com.example.emad.unknown.data.local.entities.Match_Table;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.Question_Table;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails_Table;
import com.example.emad.unknown.data.local.prefs.PreferencesHelper;
import com.example.emad.unknown.data.manager.remote.APIHelperInterface;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.OptionsWrapperModel;
import com.example.emad.unknown.data.model.QuestionAction;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.util.AppConstants;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by emad on 4/6/17.
 */


@Singleton
public class AppDataManager implements DataManager {


    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final APIHelperInterface mApiHelperInterface;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          PreferencesHelper preferencesHelper,
                          APIHelperInterface apiHelperInterface) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelperInterface = apiHelperInterface;
    }



//    @Override
//    public Observable<Boolean> writeQuestionLocalObservable(final Question question, final List<Answer> answers) {
//        return Observable.fromCallable(new Callable<Boolean>() {
//            @Override
//            public Boolean call() throws Exception {
//                question.save();
//                for(Answer ans: answers){
//                    ans.setQuestion(question);
//                    ans.save();
//                }
//                return true;
//            }
//        });
//    }

    @Override
    public Observable<Question> readNewQuestionObservable() {
        return Observable.fromCallable(new Callable<Question>() {
            @Override
            public Question call() throws Exception {
                return new Select()
                        .from(Question.class)
                        .where(Question_Table.isDuplicate.eq(false))
                        .querySingle();
            }
        });
    }

    @Override
    public Observable<List<Question>> getQuestionsListForMatch(final int listSize) {
        return Observable.fromCallable(new Callable<List<Question>>() {
            @Override
            public List<Question> call() throws Exception {
                List<Question> questions =new Select()
                        .from(Question.class)
                        .where(Question_Table.isDuplicate.eq(false))
                        .queryList();

                if (questions.size() < listSize)
                    throw new RuntimeException("there is no enough question!") ;

                return questions.subList(0, listSize);
            }
        });
    }

    @Override
    public Observable<UserDeltaDetails> getUserDeltaDetails() {
        return Observable.fromCallable(new Callable<UserDeltaDetails>() {
            @Override
            public UserDeltaDetails call() throws Exception {
                UserDeltaDetails userDeltaDetails = new Select()
                        .from(UserDeltaDetails.class)
                        .querySingle();
                if (userDeltaDetails == null)
                {
                    userDeltaDetails = new UserDeltaDetails();
                    userDeltaDetails.save();
                }
                return userDeltaDetails;
            }
        });

    }

    @Override
    public Observable<UserDetails> readUserDetailsObservable() {
        return Observable.fromCallable(new Callable<UserDetails>() {
            @Override
            public UserDetails call() throws Exception {
                UserDetails userDetails = new Select()
                        .from(UserDetails.class)
                        .querySingle();
                if (userDetails == null) {
                    userDetails = new UserDetails();
                    userDetails.save();
//                    throw new RuntimeException("userDetails cannot be null. make sure for sync.");
                }
                return userDetails;
            }
        });
    }

    @Override
    public Observable<Boolean> writeUserDetailsObservable(final UserDetails userDetails) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                UserDeltaDetails userDeltaDetails = new Select()
                        .from(UserDeltaDetails.class)
                        .querySingle();

                if (userDeltaDetails != null){
                    userDeltaDetails.refresh();
                }else {
                    userDeltaDetails = new UserDeltaDetails();
                }

                userDeltaDetails.save();

                UserDetails prevUserDetails = new Select()
                        .from(UserDetails.class)
                        .querySingle();
                if (prevUserDetails != null)
                    prevUserDetails.delete();

                userDetails.save();

                return true;
            }
        });
    }

    @Override
    public void setSignedUp(boolean situation) {
        mPreferencesHelper.setSignedUp(situation);
    }

    @Override
    public boolean isSignedUp() {
        return mPreferencesHelper.isSignedUp();
    }

    @Override
    public Timestamp getUserLastSyncDetails() {
        return mPreferencesHelper.getUserLastSyncDetails();
    }

    @Override
    public void setUserLastSyncDetails(Timestamp syncTime) {
        mPreferencesHelper.setUserLastSyncDetails(syncTime);
    }

    @Override
    public Timestamp getUserLastSyncMatches() {
        return mPreferencesHelper.getUserLastSyncMatches();
    }

    @Override
    public void setUserLastSyncMatches(Timestamp syncTime) {
        mPreferencesHelper.setUserLastSyncMatches(syncTime);
    }

    @Override
    public Timestamp getUserLastSyncQuestions() {
        return mPreferencesHelper.getUserLastSyncQuestions();
    }

    @Override
    public void setUserLastSyncQuestions(Timestamp syncTime) {
        mPreferencesHelper.setUserLastSyncQuestions(syncTime);
    }

    @Override
    public String getUsername() {
        return mPreferencesHelper.getUsername();
    }

    @Override
    public void setUsername(String username) {
        mPreferencesHelper.setUsername(username);
    }

    @Override
    public String getPassword() {
        return mPreferencesHelper.getPassword();
    }

    @Override
    public void setPassword(String password) {
        mPreferencesHelper.setPassword(password);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
    }

    @Override
    public String getRefreshToken() {
        return mPreferencesHelper.getRefreshToken();
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        mPreferencesHelper.setRefreshToken(refreshToken);
    }

    @Override
    public String getTokenType() {
        return mPreferencesHelper.getTokenType();
    }

    @Override
    public void setTokenType(String tokenType) {
        mPreferencesHelper.setTokenType(tokenType);
    }

    @Override
    public Observable<UserDetails> registerUserObservable(UserAuth userAuth) {
        return mApiHelperInterface.registerUserObservable(userAuth);
    }

    @Override
    public Observable<UserDetails> syncUserDetailsRemoteObservable(UserDeltaDetails userDeltaDetails, UserAuth userAuth) {
        return mApiHelperInterface.syncUserDetailsRemoteObservable(userDeltaDetails,userAuth);
    }

    @Override
    public Observable<QuestionsWrapper> syncQuestionsRemoteObservable(QuestionsSituationWrapper questionsSituationWrapper, UserAuth userAuth) {
        return mApiHelperInterface.syncQuestionsRemoteObservable(questionsSituationWrapper,userAuth);
    }

    @Override
    public Observable<Match> getMatchesRemoteObservable(UserAuth userAuth) {
        return mApiHelperInterface.getMatchesRemoteObservable(userAuth);
    }

    @Override
    public Observable<AuthResponse> authUser(UserAuth userAuth) {
        return mApiHelperInterface.authUser(userAuth);
    }

    @Override
    public Observable<Boolean> writeMatchLocalObservable(final Match match) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Match prevMatch = new Select().from(Match.class)
                        .where(Match_Table.id.is(match.getId()))
                        .querySingle();
                if (prevMatch != null)
                    prevMatch.delete();
                match.save();
                return true;
            }
        });
    }

    @Override
    public Observable<Match> readMatchesLocalObservable() {
        return Observable.create(new Observable.OnSubscribe<Match>(){
            @Override
            public void call(Subscriber<? super Match> subscriber) {
                List<Match> matches = new Select()
                        .from(Match.class)
                        .queryList();
                for (int i = 0 ; i < matches.size() ; i ++)
                    subscriber.onNext(matches.get(i));
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Observable<Boolean> writeQuestionLocalObservable(final List<QuestionModel> questionModels) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                for (int i = 0 ; i < questionModels.size() ; i ++) {
                    Question question = new Question(questionModels.get(i).getText(), questionModels.get(i).getCorrectAnswerInt(), false, questionModels.get(i).getId());
                    question.save();
                    for (OptionsWrapperModel optionsWrapperModel : questionModels.get(i).getOptions()) {
                        Answer answer = new Answer(optionsWrapperModel, question);
                        answer.save();
                    }

                }
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> writeMatchesInstancesLocalObservable(final MatchInstance matchInstance) {
        return  Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                matchInstance.save();
                return true;
            }
        });
    }

    @Override
    public Observable<List<MatchInstance>> readMatchInstancesLocalObservable() {
        return Observable.create(new Observable.OnSubscribe<List<MatchInstance>>() {
            @Override
            public void call(Subscriber<? super List<MatchInstance>> subscriber) {
                List<MatchInstance> matchInstances = new Select()
                        .from(MatchInstance.class)
                        .queryList();
                subscriber.onNext(matchInstances);
//                Log.d("matches size:", "::"+matchInstances.size());
//                for (int i = 0 ; i < 100 ; i++) {
//                    subscriber.onNext(matchInstances.get(i));
//                    Log.d("matches size:", ":: here :"+ i);
//                }
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Observable<Boolean> refreshDeltaDetails(final int adCoins, final int adGems, final int adXp, final int adLvl) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {

                UserDeltaDetails userDeltaDetails = new Select()
                        .from(UserDeltaDetails.class)
                        .querySingle();

                if (userDeltaDetails != null) {
                    userDeltaDetails.setCoin(userDeltaDetails.getCoin() + adCoins);
                    userDeltaDetails.setGem(userDeltaDetails.getGem() + adGems);
                    userDeltaDetails.setXp(userDeltaDetails.getXp() + adXp);
                    userDeltaDetails.setLevel(userDeltaDetails.getLevel() + adLvl);
                    userDeltaDetails.update();
                }else{
                    throw new RuntimeException("something wrong userDeltaDetails is null.");
                }

                return true;
            }
        });
    }

    @Override
    public MatchInstance readMatchInstance(long quizId) {
        MatchInstance matchInstance = new Select()
                .from(MatchInstance.class)
                .where(MatchInstance_Table.id.eq(quizId))
                .querySingle();
        if (matchInstance == null)
            throw new RuntimeException("matchInstance not found with quizId:" + quizId);
        return matchInstance;
    }

    @Override
    public QuestionsSituationWrapper getQuestionsSituation() {
        List<Question> seen = new Select()
                .from(Question.class)
                .where(Question_Table.isDuplicate.eq(true))
                .queryList();
        ArrayList<Long> seenId = new ArrayList<>();
        for(Question question: seen) {
            seenId.add(question.getQuestionServerKey());
        }

        List<Question> unseen = new Select()
                .from(Question.class)
                .where(Question_Table.isDuplicate.eq(false))
                .queryList();
        ArrayList<Long> unseenId = new ArrayList<>();
        for(Question question: unseen) {
            unseenId.add(question.getQuestionServerKey());
        }

        List<Answer> hitOptions = new Select()
                .from(Answer.class)
                .where(Answer_Table.isHit.eq(true))
                .queryList();
        ArrayList<Long> hitOptionsId = new ArrayList<>();
        for (Answer answer: hitOptions)
            hitOptionsId.add(answer.getId());

        return new QuestionsSituationWrapper(seenId, unseenId,hitOptionsId);
    }

    @Override
    public void setUserAuth(AuthResponse authResponse) {
        if (authResponse.getAccess_token() != null)
            setAccessToken(authResponse.getAccess_token());
        if (authResponse.getRefresh_token() != null)
            setRefreshToken(authResponse.getRefresh_token());
        if (authResponse.getToken_type() != null)
            setTokenType(authResponse.getToken_type());
        if (authResponse.getPassword() != null)
            setPassword(authResponse.getPassword());
        if (authResponse.getUsername() != null)
            setUsername(authResponse.getUsername());
    }

    @Override
    public void deleteSeenQuestions() {
        List<Question> seenQuestions = new Select()
                .from(Question.class)
                .where(Question_Table.isDuplicate.eq(true))
                .queryList();
        for (Question question: seenQuestions)
            question.delete();

    }

    @Override
    public long countUnseenQuestions() {
        List<Question> unseenQuestions = new Select()
                .from(Question.class)
                .where(Question_Table.isDuplicate.eq(false))
                .queryList();
        Log.d(AppConstants.Log.DBFlow,"unseen questions are: " + unseenQuestions.size() );
        return unseenQuestions.size();
    }

    @Override
    public long getUserCoin() {
        UserDetails userDetails = new Select()
                .from(UserDetails.class)
                .querySingle();
        UserDeltaDetails userDeltaDetails = new Select()
                .from(UserDeltaDetails.class)
                .querySingle();
        if (userDetails == null)
            throw new RuntimeException("userDetails is null!");
        if (userDeltaDetails == null)
        {
            userDeltaDetails = new UserDeltaDetails();
            userDeltaDetails.save();
        }


        return userDeltaDetails.getCoin() + userDetails.getCoin();
    }

    @Override
    public void consumeExtraGame(long id) {
        MatchInstance matchInstance = new Select()
                .from(MatchInstance.class)
                .where(MatchInstance_Table.id.eq(id))
                .querySingle();
        matchInstance.setConsumed(true);
        matchInstance.save();
    }

    @Override
    public void removeConsumedMatchInstances() {
        List<MatchInstance> matchInstances = new Select()
                .from(MatchInstance.class)
                .where(MatchInstance_Table.consumed.eq(true))
                .queryList();
        for (int i = 0 ; i < matchInstances.size() ; i ++)
            matchInstances.get(i).delete();
    }

    @Override
    public boolean hasGem() {
        UserDetails userDetails = new Select()
                .from(UserDetails.class)
                .querySingle();
        UserDeltaDetails userDeltaDetails = new Select()
                .from(UserDeltaDetails.class)
                .querySingle();
        return (userDeltaDetails.getGem() + userDetails.getGems())>0;
    }

}
