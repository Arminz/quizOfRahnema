package com.example.emad.unknown.data.model;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by armin on 5/2/17.
 */

public class QuestionsSituationWrapper {

    private ArrayList<Long> seenQuestions;
    private ArrayList<Long> unseen;
    private ArrayList<Long> hitOptions;


    public QuestionsSituationWrapper() {
    }

    public ArrayList<Long> getHitOptions() {
        return hitOptions;
    }

    public void setHitOptions(ArrayList<Long> hitOptions) {
        this.hitOptions = hitOptions;
    }

    public QuestionsSituationWrapper(ArrayList<Long> seen, ArrayList<Long> unseen,ArrayList<Long> hitOptions) {

        this.seenQuestions = seen;
        this.unseen = unseen;
        this.hitOptions = hitOptions;

    }

    public ArrayList<Long> getSeenQuestions() {
        return seenQuestions;
    }

    public void setSeenQuestions(ArrayList<Long> seenQuestions) {
        this.seenQuestions = seenQuestions;
    }

    public ArrayList<Long> getUnseen() {
        return unseen;
    }

    public void setUnseen(ArrayList<Long> unseen) {
        this.unseen = unseen;
    }
}
