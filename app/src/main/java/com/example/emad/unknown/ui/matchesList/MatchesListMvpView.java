package com.example.emad.unknown.ui.matchesList;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.model.GameTitle;
import com.example.emad.unknown.ui.base.MvpView;

import java.util.ArrayList;

/**
 * Created by emad on 4/17/17.
 */

public interface MatchesListMvpView extends MvpView {

    void setMatchesList(ArrayList<Match> matches);
    
}