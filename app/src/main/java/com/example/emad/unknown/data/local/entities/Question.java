package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;
import java.util.List;

/**
 * Created by emad on 3/15/17.
 */

@Table(database = DataBase.class)
public class Question extends BaseModel{

    @Column
    @PrimaryKey(autoincrement = true)
    int id;

    @Column
    String question;

    @Column
    int correctAnswer;

    @Column
    boolean isDuplicate;

    @Column
    int userAnswer;

    @Column
    Date useDate;

    @Column
    long questionServerKey;

    List<Answer> questionAnswers;

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "questionAnswers")
    public List<Answer> getQuestionAnswers() {
        if (questionAnswers == null || questionAnswers.isEmpty()) {
            questionAnswers = new Select()
                    .from(Answer.class)
                    .where(Answer_Table.question_id.eq(id))
                    .queryList();
        }
        return questionAnswers;
    }

    public Question(){}

    public Question(String question, int correctAnswer, boolean isDuplicate, long questionServerKey) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.isDuplicate = isDuplicate;
        this.questionServerKey = questionServerKey;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public void setQuestionAnswers(List<Answer> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }

    public boolean isDuplicate() {
        return isDuplicate;
    }

    public void setDuplicate(boolean duplicate) {
        isDuplicate = duplicate;
    }

    public int getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(int userAnswer) {
        this.userAnswer = userAnswer;
    }

    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }

    public long getQuestionServerKey() {
        return questionServerKey;
    }

    public void setQuestionServerKey(long questionServerKey) {
        this.questionServerKey = questionServerKey;
    }
}
