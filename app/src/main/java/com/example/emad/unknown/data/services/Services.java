package com.example.emad.unknown.data.services;

import android.app.AlarmManager;

/**
 * Created by armin on 4/11/17.
 */

public interface Services {
    String LOG = "Service";
    interface MakeExtraGameProperties {
        int HOUR_OF_DAY = 11;
        int MINUTE = 22;
        int SECOND = 0;

//        long INTERVAL = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
        int REQUEST_CODE = 0;
        int FLAG = 0;
//        int FLAG = PendingIntent.FLAG_NO_CREATE;
        int SERVICE_TYPE = AlarmManager.RTC_WAKEUP;
        interface PROBABILITIES {
            double EXTRA_GAME1_CHANCE = 0.5;
            double EXTRA_GAME2_CHANCE = 0.5;
        }
    }


}
