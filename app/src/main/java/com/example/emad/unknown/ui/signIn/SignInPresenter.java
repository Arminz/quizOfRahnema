package com.example.emad.unknown.ui.signIn;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.OptionsWrapperModel;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 5/3/17.
 */

public class SignInPresenter<V extends SignInMvpView> extends BasePresenter<V>
        implements SignInMvpPresenter<V> {

    @Inject
    public SignInPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void signIn(final String username, final String password) {
        if (username.equals("offline") && password.trim().isEmpty())
        {
            generateFakeMatches();
            return;
        }
        if (username.trim().isEmpty())
            getMvpView().signInError(AppConstants.SignErrors.usernameEmpty);
        if (password.trim().isEmpty())
            getMvpView().signInError(AppConstants.SignErrors.passwordEmpty);
        if (!NetworkUtil.isNetworkConnected(getContext()))
            getMvpView().signInError(AppConstants.SignErrors.NO_INTERNET_CONNECTION);
        if (username.trim().isEmpty() || password.trim().isEmpty())
            return;

        if (NetworkUtil.isNetworkConnected(getContext())) {
//            getMvpView().showLoading();
            getMvpView().disableSignInBtn();
            UserAuth userAuth = new UserAuth();
            userAuth.setUsername(username);
            userAuth.setPassword(password);
            userAuth.setToken(AppConstants.AUTH_KEY);
            userAuth.setType(AppConstants.GRANT_TYPE.PASSWORD);
            getDataManager().authUser(userAuth).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<AuthResponse>() {
                        @Override
                        public void onCompleted() {
                            Log.d(AppConstants.Log.RETROFIT,"signIn onCompleted register");
                            getMvpView().enableSignInBtn();
                            getDataManager().setSignedUp(true);
                            getMvpView().navigateToMain();
                        }

                        @Override
                        public void onError(Throwable e) {
                            getMvpView().enableSignInBtn();
                            Log.d(AppConstants.Log.RETROFIT,"signIn onError : " + e.toString());
                            getMvpView().signInError(e.getMessage());

                        }

                        @Override
                        public void onNext(AuthResponse authResponse) {
                            Log.d(AppConstants.Log.RETROFIT,"signIn onNext : " + authResponse.getAccess_token());
                            authResponse.setPassword(password);
                            authResponse.setUsername(username);
                            getDataManager().setUserAuth(authResponse);
                        }
                    });
        }else {
            getMvpView().signInError(AppConstants.SignErrors.NO_INTERNET_CONNECTION);
        }
    }


    void makeFakeQuestions()
    {
        ArrayList<Integer> deleted = new ArrayList<>();
        ArrayList<QuestionModel> questions = new ArrayList<>();
        for (int i = 0 ; i < 100 ; i ++)
        {
            QuestionModel questionModel = new QuestionModel();
            Random random = new Random();
            int a = random.nextInt() % 10;
            int b = random.nextInt() % 10;
            questionModel.setText(a + " + " + b + " = ?");

            ArrayList<OptionsWrapperModel> options = new ArrayList<>();
            OptionsWrapperModel option1 = new OptionsWrapperModel();
            option1.setText(String.valueOf(a+b));
            option1.setNumberOfHit("0");
            option1.setId(0);

            OptionsWrapperModel option2 = new OptionsWrapperModel();
            option2.setText(String.valueOf(random.nextInt()%10));
            option2.setId(1);
            option2.setNumberOfHit("0");

            boolean c = random.nextBoolean();
            if (!c) {
                options.add(option1);
                options.add(option2);
            }
            else if (c){
                options.add(option2);
                options.add(option1);
            }
            else
                throw new RuntimeException("random err...");

            questionModel.setOptions(options);

            questionModel.setCorrectAnswer(0);
            questionModel.setNumberOfSeen(0);

            questions.add(questionModel);

        }

        QuestionsWrapper questionsWrapper = new QuestionsWrapper(questions,deleted);

        saveQuestions(questionsWrapper);
    }


    void saveQuestions(QuestionsWrapper questionsWrapper)
    {
        Gson gson = new Gson();
        String json = gson.toJson(questionsWrapper);
//                            Toast.makeText(context, "new questions : " + json, Toast.LENGTH_SHORT).show();
        Log.d(AppConstants.Log.RETROFIT, "getQuestions onNext : " + json);
        getDataManager().writeQuestionLocalObservable(questionsWrapper.getQuestions()).subscribeOn(Schedulers.io()).subscribe(new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d(AppConstants.Log.DBFlow, "write new questions complete");
                getMvpView().enableSignInBtn();
//                getDataManager().setSignedUp(true);
                getMvpView().navigateToMain();
            }

            @Override
            public void onError(Throwable e) {
                Log.d(AppConstants.Log.DBFlow, "write new questions error : " + e.getMessage());

            }

            @Override
            public void onNext(Boolean aBoolean) {
                Log.d(AppConstants.Log.DBFlow, "write new questions onNext:" + aBoolean);

            }
        });

    }

    void generateFakeMatches()
    {
        Match match1 = new Match();//سه ستاره
        match1.setXpReward(5);
        match1.setCoinReward(50);
        match1.setEnterCoin(100);
        match1.setGemReward(1);
        match1.setDecreasingTime(3);
        match1.setNumberOfCreate(1);
        match1.setMaximumTime(25);
        match1.setNumberOfQuestion(30);
        match1.setType(1);
        saveMatch(match1);

        Match match2 = new Match();//فازی
        match2.setXpReward(5);
        match2.setCoinReward(50);
        match2.setEnterCoin(50);
        match2.setGemReward(1);
        match2.setDecreasingTime(10);
        match2.setNumberOfCreate(2);
        match2.setMaximumTime(10);
        match2.setNumberOfQuestion(30);
        match2.setType(2);
        saveMatch(match2);


    }
    void saveMatch(final Match match)
    {
        Gson gson = new Gson();
        String json = gson.toJson(match);
//                            Toast.makeText(context, "getMatches : " + json, Toast.LENGTH_SHORT).show();
        Log.d(AppConstants.Log.RETROFIT, "getMatches onNext : " + json);
        getDataManager().writeMatchLocalObservable(match).subscribeOn(Schedulers.io()).subscribe(new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d(AppConstants.Log.DBFlow,"write match complete");
                if (match.getType() == 2)
                    fakeUserDetails();
//                                    Intent intent = new Intent(context,InstantiateMatchReceiver.class);
//                                    context.sendBroadcast(intent);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(AppConstants.Log.DBFlow,"write match ");

            }

            @Override
            public void onNext(Boolean aBoolean) {
                Log.d(AppConstants.Log.DBFlow,"write match onNext:" + aBoolean);

            }
        });

    }
    void fakeUserDetails()
    {
        UserDetails userDetails = new UserDetails();
        userDetails.setCoin(400);
        userDetails.setGems(10);
        userDetails.setLevel(1);
        userDetails.setXp(0);
        getDataManager().writeUserDetailsObservable(userDetails).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.d(AppConstants.Log.DBFlow,"saveUserDetails -> onCompleted. ");
                        makeFakeQuestions();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(AppConstants.Log.DBFlow,"saveUserDetails -> onError : " + e.getMessage());
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.d(AppConstants.Log.DBFlow,"saveUserDetails -> onNext.");

                    }
                });

    }
}
