package com.example.emad.unknown.data.manager.remote.sockets;

import android.util.Log;

import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.APIError;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.ErrorUtils;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.util.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by armin on 5/2/17.
 */

public class RegisterUserSocket implements Observable.OnSubscribe<UserDetails> {

    private UserAuth userAuth;
    public static Observable<UserDetails> getObservable(UserAuth userAuth){
        return Observable.create(new RegisterUserSocket(userAuth));
    }

    private RegisterUserSocket(UserAuth userAuth){
        this.userAuth = userAuth;
    }

    @Override
    public void call(final Subscriber<? super UserDetails> subscriber) {
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<UserDetails> responseCall = api.registerUser();
        responseCall.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                if (response.isSuccessful()) {
                    Log.d(AppConstants.Log.RETROFIT,response.raw().toString());
                    subscriber.onNext(response.body());
                    subscriber.onCompleted();

                }
                else {
                    APIError error = ErrorUtils.parseError(response);
                    Log.d(AppConstants.Log.RETROFIT,error.message());
                    if (response.code() == 500)
                        subscriber.onError(new Throwable(AppConstants.SignErrors.USED_USERNAME));
                    else
                        subscriber.onError(new Throwable(error.message()));
                }

            }

            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"error connecting to server...");
                subscriber.onError(t);
            }
        });
    }
}
