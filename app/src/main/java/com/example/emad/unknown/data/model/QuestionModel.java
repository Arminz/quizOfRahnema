package com.example.emad.unknown.data.model;

import java.util.ArrayList;

/**
 * Created by emad on 5/6/17.
 */

public class QuestionModel {

    private long id;

    private String text;

    private int difficulty;

    private long correctAnswer;

    private long numberOfSeen;

    private ArrayList<OptionsWrapperModel> options;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public long getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(long correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public long getNumberOfSeen() {
        return numberOfSeen;
    }

    public void setNumberOfSeen(long numberOfSeen) {
        this.numberOfSeen = numberOfSeen;
    }

    public ArrayList<OptionsWrapperModel> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionsWrapperModel> options) {
        this.options = options;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCorrectAnswerInt(){

        int ans = -1;

        for (int i = 0 ; i < options.size() ; i++){
            if (options.get(i).getId() == correctAnswer){
                ans = i;
            }
        }

        return ans;
    }
}
