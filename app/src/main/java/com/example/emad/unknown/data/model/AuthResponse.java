package com.example.emad.unknown.data.model;

import com.google.gson.Gson;

/**
 * Created by emad on 5/13/17.
 */

public class AuthResponse {

    // sorry for params name :(( ;

    public String access_token;

    public String token_type;

    public String refresh_token;

    public String username;

    public String password;

    public AuthResponse() {}

    public AuthResponse(String access_token, String token_type, String refresh_token,
               String username, String password) {
        this.access_token = access_token;
        this.token_type = token_type;
        this.refresh_token = refresh_token;
        this.username = username;
        this.password = password;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }


    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
