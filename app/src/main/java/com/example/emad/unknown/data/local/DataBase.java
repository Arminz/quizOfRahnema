package com.example.emad.unknown.data.local;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by emad on 3/15/17.
 */

@Database(name = DataBase.NAME, version = DataBase.VERSION)
public class DataBase {

    public static final String NAME = "MyDataBase";

    public static final int VERSION = 1;

}
