package com.example.emad.unknown.ui.register;

import android.content.Context;

import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by emad on 5/3/17.
 */

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {

    @Inject
    public RegisterPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public boolean isSignedUp() {
        return getDataManager().isSignedUp();
    }

}
