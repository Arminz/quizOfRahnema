package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import static com.example.emad.unknown.data.local.entities.ExtraGamesDefaultValues.Type2.*;


/**
 * Created by armin on 4/9/17.
 */

@Table(database = DataBase.class)
public class ExtraGame2 extends BaseModel {
    @Column
    @PrimaryKey(autoincrement = true)
    int id;

    @Column(defaultValue = REWARD_COIN)
    int rewardCoin;

    @Column(defaultValue = START_COST)
    int startCost;

    @Column(defaultValue = TIME_BOUND_PER_QUESTION)
    int timeBoundPerQuestion;

    public int getId() {
        return id;
    }

    public int getRewardCoin() {
        return rewardCoin;
    }

    public int getStartCost() {
        return startCost;
    }

    public int getTimeBoundPerQuestion() {
        return timeBoundPerQuestion;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRewardCoin(int rewardCoin) {
        this.rewardCoin = rewardCoin;
    }

    public void setStartCost(int startCost) {
        this.startCost = startCost;
    }

    public void setTimeBoundPerQuestion(int timeBoundPerQuestion) {
        this.timeBoundPerQuestion = timeBoundPerQuestion;
    }
}
