package com.example.emad.unknown.data.model;

/**
 * Created by armin on 5/23/17.
 */

public class MatchReward {

    private int gem;
    private int xp;
    private int coin;

    private int bonusGem;
    private int bonusXp;
    private int bonusCoin;

    public int getGem() {
        return gem;
    }

    public void setGem(int gem) {
        this.gem = gem;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getBonusGem() {
        return bonusGem;
    }

    public void setBonusGem(int bonusGem) {
        this.bonusGem = bonusGem;
    }

    public int getBonusXp() {
        return bonusXp;
    }

    public void setBonusXp(int bonusXp) {
        this.bonusXp = bonusXp;
    }

    public int getBonusCoin() {
        return bonusCoin;
    }

    public void setBonusCoin(int bonusCoin) {
        this.bonusCoin = bonusCoin;
    }




}
