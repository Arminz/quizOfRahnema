package com.example.emad.unknown.injection.module;

import android.app.Application;
import android.content.Context;

import com.example.emad.unknown.data.local.prefs.AppPreferencesHelper;
import com.example.emad.unknown.data.local.prefs.PreferencesHelper;
import com.example.emad.unknown.data.manager.AppDataManager;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.manager.remote.APIHelper;
import com.example.emad.unknown.data.manager.remote.APIHelperInterface;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.injection.PreferenceInfo;
import com.example.emad.unknown.util.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emad on 4/6/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }


    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    APIHelperInterface provideAPIHelper(APIHelper apiHelper) {
        return apiHelper;
    }

}
