package com.example.emad.unknown.data.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.emad.unknown.UnknownApplication;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.injection.component.DaggerReceiverComponent;
import com.example.emad.unknown.injection.component.ReceiverComponent;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by armin on 5/20/17.
 */

public class UpdateTokensReceiver extends BroadcastReceiver {

    @Inject
    DataManager dataManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(AppConstants.Log.RECEIVER, "UpdateTokensReceiver triggered now...");

        ReceiverComponent component = DaggerReceiverComponent.builder()
                .applicationComponent(((UnknownApplication) context.getApplicationContext()).getComponent())
                .build();
        component.inject(this);

        if (NetworkUtil.isNetworkConnected(context)) {
            updateAccessToken();
        }


    }
    private void updateAccessToken() {

        Log.d(AppConstants.Log.RETROFIT, "going to update accessToken...(by UpdateTokenReceiver)");

        UserAuth userAuth = new UserAuth();
        userAuth.setRefreshToken(dataManager.getRefreshToken());
        userAuth.setType(AppConstants.GRANT_TYPE.REFRESH_TOKEN);
        userAuth.setType(AppConstants.GRANT_TYPE.REFRESH_TOKEN);
        userAuth.setTryToUpdateToken(false);
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<AuthResponse> updateAccessToken = api.updateAccessToken(AppConstants.GRANT_TYPE.REFRESH_TOKEN,userAuth.getRefreshToken());
        updateAccessToken.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                if (response.isSuccessful())
                {
                    Log.d(AppConstants.Log.RETROFIT,"updateAccessToken -> onResponse -> Successful: " + response.message());
                    AuthResponse authResponse = response.body();
                    dataManager.setAccessToken(authResponse.getAccess_token());
                    dataManager.setRefreshToken(authResponse.getRefresh_token());
                }
                else {
                    Log.d(AppConstants.Log.RETROFIT,"updateAccessToken -> onResponse -> unSuccessful: " + response.message());
                    updateRefreshToken();


                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"updateAccessToken -> onFailure: " + t.getMessage());

            }
        });
    }

    private  void updateRefreshToken() {

        Log.d(AppConstants.Log.RETROFIT, "going to update refreshToken...(by UpdateTokensReceiver)");

        UserAuth userAuth = new UserAuth();
        userAuth.setType(AppConstants.GRANT_TYPE.PASSWORD);
        userAuth.setUsername(dataManager.getUsername());
        userAuth.setPassword(dataManager.getPassword());
        userAuth.setTryToUpdateToken(false);
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<AuthResponse> updateAccessToken = api.updateRefreshToken(AppConstants.GRANT_TYPE.PASSWORD,userAuth.getUsername(),userAuth.getPassword());
        updateAccessToken.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                if (response.isSuccessful())
                {
                    Log.d(AppConstants.Log.RETROFIT,"updateRefreshToken -> onResponse -> successful: " + response.message());
                    AuthResponse authResponse = response.body();
                    dataManager.setAccessToken(authResponse.getAccess_token());
                    dataManager.setRefreshToken(authResponse.getRefresh_token());
                }
                else {
                    Log.d(AppConstants.Log.RETROFIT,"updateRefreshToken -> onResponse -> unSuccessful: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"updateRefreshToken -> onFailure: " + t.getMessage());
            }
        });

    }
}
