package com.example.emad.unknown.data.manager.remote;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.QuestionAction;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by emad on 5/2/17.
 */

public interface APIHelperInterface {

    Observable<UserDetails> registerUserObservable(UserAuth userAuth);

    Observable<UserDetails> syncUserDetailsRemoteObservable(UserDeltaDetails userDeltaDetails, UserAuth userAuth);

    Observable<QuestionsWrapper> syncQuestionsRemoteObservable(QuestionsSituationWrapper questionsSituationWrapper, UserAuth userAuth);

    Observable<Match> getMatchesRemoteObservable(UserAuth userAuth);

    Observable<AuthResponse> authUser(UserAuth userAuth);

//    Observable<List<QuestionModel>> getQuestionsRemoteObservable(UserAuth userAuth);

}
