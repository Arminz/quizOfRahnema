package com.example.emad.unknown.ui.quiz;

import com.example.emad.unknown.injection.PerActivity;
import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/17/17.
 */

@PerActivity
public interface QuizMvpPresenter<V extends QuizMvpView> extends MvpPresenter<V> {

    void refreshDetails();

}
