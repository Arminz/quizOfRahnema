package com.example.emad.unknown.ui.Alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by armin on 4/12/17.
 */

public class NotificationHandler {
    public static void Notify(Context context, String message)
    {

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(NOTIFICATION_SERVICE);
        Notification  n = new Notification.Builder(context)
                .setContentTitle("New extraGame ")
                .setContentText(message).setSmallIcon(android.support.v7.appcompat.R.drawable.notification_icon_background).build();
        notificationManager.notify(0,n);
    }
}
