package com.example.emad.unknown.ui.main;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.injection.PerActivity;
import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/6/17.
 */

@PerActivity
public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

//    int getUserCoins();
//
//    void setUserCoins(int x);
//
//    int getUserGems();
//
//    void setUserGems(int x);
//
//    int getUserRank();
//
//    void setUserRank(int x);
//
//    int getUserXp();
//
//    void setUserXp(int xp);

    void saveUserDetails(UserDetails userDetails);

    boolean isSignedUp();

    void refreshUserDetails();

    void signUp();

    void getMatches();

    void getQuestions();

    void updateTokens();

    void refreshDetailView();

    void instantiateMatches();
}
