package com.example.emad.unknown.util;

import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.model.MatchReward;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by armin on 5/23/17.
 */

public class MatchPhasesConstants {
//
//    public static MatchReward matchFinishReward(MatchInstance matchInstance, int correctAnswers){
//        if (matchInstance.getType() == 1)
//            return secondMatchFinishReward(matchInstance, correctAnswers);
//        else if (matchInstance.getType() == 2)
//            return firstMatchFinishReward(matchInstance,correctAnswers);
//        else if (matchInstance.getType() == 0)
//            return normalMatchReward(matchInstance,correctAnswers);
//        else return null;
//
//    }
//
    public static ArrayList<Integer> getPhases(MatchInstance matchInstance )
    {
        if (matchInstance.getType() == 2) {
            ArrayList<Integer> phases = new ArrayList<>();
            phases.add(3);
            phases.add(7);
            phases.add(12);
            phases.add(18);
            return phases;
        }
        else if (matchInstance.getType() == 1)
        {
            ArrayList<Integer> phases = new ArrayList<>();
            phases.add(5);
            phases.add(7);
            phases.add(9);
            return phases;

        }
        else if (matchInstance.getType() == 0)
        {
            ArrayList<Integer> phases = new ArrayList<>();
            phases.add(matchInstance.getNumberOfQuestion() - 2);
            return phases;
        }
        else
            throw new RuntimeException("matchInstance illegal type: " + matchInstance.getType());

    }
//    private static MatchReward secondMatchFinishReward(MatchInstance matchInstance, int correctAnswers)
//    {
//        MatchReward matchReward = new MatchReward();
//
//
//
//        ArrayList<Integer> phases = getPhases(matchInstance);
//        int phaseAchieve = 0;
//        for (int i = 0 ; i <= 4 ; i ++)
//            if (correctAnswers >= phases.get(i))
//                phaseAchieve = i;
//
//        int xpPerCorrectAnswer = matchInstance.getXpReward();
//        matchReward.setXp(xpPerCorrectAnswer * correctAnswers);
//        matchReward.setBonusXp(xpPerCorrectAnswer * phases.get(phaseAchieve));
//
//        matchReward.setCoin(phaseAchieve * matchInstance.getCoinReward());
//        matchReward.setBonusCoin(0);
//
//        matchReward.setGem(phaseAchieve);
//        matchReward.setBonusGem(random(phaseAchieve));
//
//        return matchReward;
//
//    }
//
//    private static MatchReward firstMatchFinishReward(MatchInstance matchInstance, int correctAnswers) {
//
//        MatchReward matchReward = new MatchReward();
//
//
//
//        ArrayList<Integer> phases();
//        int phaseAchieve = 0;
//        for (int i = 0 ; i <= 4 ; i ++)
//            if (correctAnswers >= phases.get(i))
//                phaseAchieve = i;
//
//        int xpPerCorrectAnswer = matchInstance.getXpReward();
//        matchReward.setXp(xpPerCorrectAnswer * correctAnswers);
//        matchReward.setBonusXp(xpPerCorrectAnswer * phases.get(phaseAchieve));
//
//        matchReward.setCoin(phaseAchieve * matchInstance.getCoinReward());
//        matchReward.setBonusCoin(0);
//
//        matchReward.setGem(phaseAchieve);
//        matchReward.setBonusGem(0);
//
//        return matchReward;
//    }
//    private static MatchReward normalMatchReward(MatchInstance matchInstance,int correctAnswer)
//    {
//        MatchReward matchReward = new MatchReward();
//
//        matchReward.setXp(0);
//        matchReward.setBonusXp(0);
//
//        if (correctAnswer >= matchInstance.getNumberOfQuestion()-3)
//            matchReward.setCoin(matchInstance.getCoinReward());
//        matchReward.setBonusCoin(0);
//
//        matchReward.setGem(0);
//        matchReward.setBonusGem(0);
//
//        return matchReward;
//    }
//    public static int xpPerQuestion(MatchInstance matchInstance)
//    {
//        return matchInstance.getXpReward();
//    }
//    public static int timePerQuestions(MatchInstance matchInstance)
//    {
//        return matchInstance.getDecreasingTime();
//    }
//    public static int timePerPhase(MatchInstance matchInstance)
//    {
//        return matchInstance.getDecreasingTime();
//    }
//
}
