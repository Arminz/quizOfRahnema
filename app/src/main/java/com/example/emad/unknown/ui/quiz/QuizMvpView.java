package com.example.emad.unknown.ui.quiz;

import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.ui.base.MvpView;

/**
 * Created by emad on 4/17/17.
 */

public interface QuizMvpView extends MvpView {

    void showNormalQuiz();

    void showQuizTypeOne(long quizId);

    void showQuizTypeTwo(long quizId);

    void refreshUserDetailsView(UserDetails userDetails);

    void increaseXP(int deltaXP);

    void increaseCoin(int deltaCoin);

    void increaseGem(int deltaGem);

}
