package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by armin on 5/6/17.
 */

@Table(database = DataBase.class)
public class Match extends BaseModel{

    @Column
    @PrimaryKey(autoincrement = true)
    private int localId;

    @Column
    private int id;

    @Column
    private int type;

    @Column
    private int maximumTime;

    @Column
    private int decreasingTime;

    @Column
    private int numberOfQuestion;

    @Column
    private int gemReward;

    @Column
    private int coinReward;

    @Column
    private int xpReward;

    @Column
    private int enterCoin;

    @Column
    private int numberOfCreate;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMaximumTime() {
        return maximumTime;
    }

    public void setMaximumTime(int maximumTime) {
        this.maximumTime = maximumTime;
    }

    public int getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public void setNumberOfQuestion(int numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

    public int getGemReward() {
        return gemReward;
    }

    public void setGemReward(int gemReward) {
        this.gemReward = gemReward;
    }

    public int getCoinReward() {
        return coinReward;
    }

    public void setCoinReward(int coinReward) {
        this.coinReward = coinReward;
    }

    public int getXpReward() {
        return xpReward;
    }

    public void setXpReward(int xpReward) {
        this.xpReward = xpReward;
    }

    public int getEnterCoin() {
        return enterCoin;
    }

    public void setEnterCoin(int enterCoin) {
        this.enterCoin = enterCoin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDecreasingTime() {
        return decreasingTime;
    }

    public void setDecreasingTime(int decreasingTime) {
        this.decreasingTime = decreasingTime;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public int getNumberOfCreate() {
        return numberOfCreate;
    }

    public void setNumberOfCreate(int numberOfCreate) {
        this.numberOfCreate = numberOfCreate;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
