//package com.example.emad.unknown.data.manager.remote.sockets;
//
///**
// * Created by armin on 5/6/17.
// */
//
//import android.util.Log;
//
//import com.example.emad.unknown.data.local.entities.Question;
//import com.example.emad.unknown.data.model.QuestionModel;
//import com.example.emad.unknown.data.model.UserAuth;
//import com.example.emad.unknown.data.remote.api.API;
//import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.APIError;
//import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.ErrorUtils;
//import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
//import com.example.emad.unknown.util.AppConstants;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import rx.Observable;
//import rx.Subscriber;
///**
// * Created by armin on 5/6/17.
// */
//
//
///**
// * Created by armin on 5/6/17.
// */
//
//public class QuestionsRemoteSocket implements Observable.OnSubscribe<List<QuestionModel>> {
//
//    private UserAuth userAuth;
//
//    private QuestionsRemoteSocket(UserAuth userAuth){this.userAuth = userAuth;}
//
//    public static Observable<List<QuestionModel>> getObservable(UserAuth userAuth){return Observable.create(new QuestionsRemoteSocket(userAuth));}
//
//
//    @Override
//    public void call(final Subscriber<? super List<QuestionModel>> subscriber) {
//        API api = ServiceGenerator.createService(API.class,userAuth);
//        Call<ArrayList<QuestionModel>> arrayListCall = api.getQuestions();
//        arrayListCall.enqueue(new Callback<ArrayList<QuestionModel>>() {
//            @Override
//            public void onResponse(Call<ArrayList<QuestionModel>> call, Response<ArrayList<QuestionModel>> response) {
//                if (response.isSuccessful()){
//                    Log.d(AppConstants.Log.RETROFIT,response.raw().toString());
//                    ArrayList<QuestionModel> questionModels = response.body();
//                    subscriber.onNext(questionModels);
//                    subscriber.onCompleted();
//                }
//                else
//                {
//                    APIError error = ErrorUtils.parseError(response);
//                    Log.d(AppConstants.Log.RETROFIT,error.message());
//                    subscriber.onError(new Throwable(error.message()));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ArrayList<QuestionModel>> call, Throwable t) {
//                Log.d(AppConstants.Log.RETROFIT,"error connecting to server...");
//                subscriber.onError(t);
//
//
//            }
//        });
//
//
//    }
//}
