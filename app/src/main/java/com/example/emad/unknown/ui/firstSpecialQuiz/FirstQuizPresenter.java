package com.example.emad.unknown.ui.firstSpecialQuiz;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.data.local.entities.Answer;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.HitResponse;
import com.example.emad.unknown.util.MatchSituation;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/24/17.
 */

public class FirstQuizPresenter<V extends FirstQuizMvpView> extends BasePresenter<V>
        implements FirstQuizMvpPresenter<V> {

    private int mSickTimer;

    private int maxTime;

    private Subscription subscription;

    private MatchSituation matchSituation;

    @Inject
    public FirstQuizPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    private MatchInstance matchInstance;

    @Override
    public void startQuiz(long quizId) {

        matchInstance = getDataManager().readMatchInstance(quizId);

        matchSituation = new MatchSituation(matchInstance);


        maxTime = matchInstance.getMaximumTime();

        setTimer(maxTime);
        getMvpView().refreshTimeView(mSickTimer, maxTime);

        getQuestion();
        startTimer();
    }

    @Override
    public void getQuestion() {
        getMvpView().showLoading();

        if (matchSituation.getPassedQuestions() >= matchInstance.getNumberOfQuestion()) {
            finishQuiz();
            getMvpView().hideLoading();
            return;
        }


        getDataManager().readNewQuestionObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Question>() {
                    @Override
                    public void onCompleted() {
                        Log.d(AppConstants.Log.DBFlow, "readNewQuestion -> onCompleted");
                        getMvpView().hideLoading();

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(AppConstants.Log.DBFlow, "readNewQuestion -> onError : " + e.getMessage());
                        Toast.makeText(getContext(),"سوالات تمام شده است.",Toast.LENGTH_SHORT).show();
//                        getMvpView().onError("can't load new question");
                        getMvpView().hideLoading();
                        finishQuiz();
                    }

                    @Override
                    public void onNext(Question question) {

                        question.setDuplicate(true);
                        question.setUseDate(new Date());
                        question.update();

                        getMvpView().refreshQuestionView(question, matchSituation.getPassedQuestions(),getDataManager().hasGem());
                        getMvpView().hideLoading();

                    }
                });
    }

    private void finishQuiz() {

        unSubscribe();

//        final int finishGainXp,finishGainGem;

//        finishGainXp = mSickTimer * matchSituation.getCorrectAnswers() * matchInstance.getXpReward();
//        finishGainGem = Math.min(matchInstance.getGemReward(),matchSituation.getCorrectAnswers());

        getDataManager().refreshDeltaDetails(matchSituation.getCoinOne() + matchSituation.getPhaseCoin(),
                matchSituation.getGemOne() + matchSituation.getPhaseGem(),
                matchSituation.getXpOne() + matchSituation.getPhaseXp(),
                0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onCompleted" );
                        getMvpView().showResult(matchSituation);

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onNext: " + e.getMessage());
//                        Toast.makeText(getContext(),"مشکلی در اضافه کردن جوایز به وجود آمده.",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        Log.d(AppConstants.Log.DBFlow,"refreshUserDetails -> onNext: " + aBoolean);

                    }
                });



    }

    @Override
    public void questionAnswered(Question question, int userAnswer) {

        if (question.getCorrectAnswer() == userAnswer) {
            HitResponse hitResponse = matchSituation.hitCorrect();
            if (hitResponse.isPhaseUpgraded())
                getMvpView().setPhase(matchSituation.getCurrentPhase());
//            Toast.makeText(getContext(),hitResponse.toString(),Toast.LENGTH_SHORT).show();
            Log.d(AppConstants.Log.GAME_SITUATION,"questionAnswered -> true : " + hitResponse.toString());
//            getDataManager().getUserDeltaDetails()
//                    .subscribeOn(Schedulers.io()).
//                    observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Action1<UserDeltaDetails>() {
//                        @Override
//                        public void call(UserDeltaDetails userDeltaDetails) {
////                            userDeltaDetails.incrementXp(matchInstance.getXpReward());
////                            userDeltaDetails.save();
//                        }
//                    });
//            getMvpView().increaseGem(matchInstance.getGemReward());
//            getMvpView().increaseXP(matchInstance.getXpReward());

        }
        else {
            HitResponse hitResponse = matchSituation.hitWrong();
            Log.d(AppConstants.Log.GAME_SITUATION,"questionAnswered -> false : " + hitResponse.toString());




        }

        question.setUserAnswer(userAnswer);
        question.update();

        Answer hitAnswer = question.getQuestionAnswers().get(userAnswer);
        hitAnswer.setIsHit(true);
        hitAnswer.update();

    }

    @Override
    public void startTimer(){
        subscription = getCountDownTimer().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        mSickTimer--;
                        Log.d("timer", "timer: "+mSickTimer);
                        if (mSickTimer == 0) {
                            finishQuiz();
                            unSubscribe();
                        }
                    }
                });
    }

    public void unSubscribe(){
        if (!subscription.isUnsubscribed())
            subscription.unsubscribe();
    }

    @Override
    public void setTimer(int timeInSec){
        mSickTimer = timeInSec;
    }

    @Override
    public void addTimeToTimer(int sec){
        mSickTimer += sec;
    }

    public Observable<Long> getCountDownTimer(){
        return Observable.interval(1, TimeUnit.SECONDS, Schedulers.io());
    }

    @Override
    public void onDetach() {
        unSubscribe();
        super.onDetach();
    }
    @Override
    public void onUseGem() {
        getDataManager().refreshDeltaDetails(0,-1,0,0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }


}
