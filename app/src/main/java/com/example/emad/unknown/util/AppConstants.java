package com.example.emad.unknown.util;

/**
 * Created by emad on 4/6/17.
 */

public final class AppConstants {

    private AppConstants(){}

    public static final String PREF_NAME = "user_pref";

    public static final String QUIZ_TYPE = "QUIZ_TYPE";

    public static final String QUIZ_ID = "QUIZ_ID";

    public static final String AUTH_KEY = "Basic c2l5YXZhc2g6MTIzNDU2MjE=";

    public static final int NORMAL_QUIZ = 0;

    public static final int QUIZ_TYPE_ONE = 1;

    public static final int QUIZ_TYPE_TWO = 2;

    public static final int QUESTION_ACTION_DELETE = 0;

    public static final int QUESTION_ACTION_UPDATE = 1;

//    public static final String URL = "http://192.168.1.3:8080/";

    public static final String URL = "http://192.168.43.46:8080/";


    public static class SignErrors {

        public final static String usernameEmpty = "USERNAME_EMPTY";

        public final static String passwordEmpty = "PASSWORD_EMPTY";

        public final static String NO_INTERNET_CONNECTION = "NO_INTERNET_CONNECTION";

        public final static String ERROR_CONNECTING_SERVER = "ERROR_CONNECTING_SERVER";

        public final static String WRONG_PASSWORD_USERNAME = "WRONG_PASSWORD_USERNAME";

        public final static String USED_USERNAME = "USED_USERNAME";
    }

    public static class Log{

        public final static String RETROFIT = "retrofit";

        public final static String DBFlow = "dbFlow";

        public final static String ALGORITHM_FLOW = "algorithmFlow";

        public final static String RECEIVER = "receiver";

        public final static String QUIZ_FLOW = "quiz";

        public final static String GAME_SITUATION = "gameSituation";


    }

    public static class GRANT_TYPE {

        public static String REFRESH_TOKEN = "refresh_token";

        public static String PASSWORD = "password";

        public static String ACCESS_TOKEN = "access_token";

    }

    public static class INTERVALS{

//        public static final long INSTANTIATE_MATCH = 60 * 1000;

        public static final long INSTANTIATE_MATCH =  3600 * 60 * 1000;

        public static final long SYNC_MATCHES = 3600 * 60 * 1000;

        public static final long UPDATE_TOKEN = 5 * 60 * 1000;

        public static long SYNC_QUESTIONS = 60 * 1000;

    }

    public interface ReceiverRequestCodes {

        int GET_MATCHES = 0;

        int GET_QUESTIONS = 1;

        int INSTANTIATE_MATCHES = 2;

        int UPDATE_TOKENS = 3;
    }

}
