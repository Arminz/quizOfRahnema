package com.example.emad.unknown.data.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.emad.unknown.UnknownApplication;
import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.MatchInstance_Table;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.services.Services;
import com.example.emad.unknown.injection.component.DaggerReceiverComponent;
import com.example.emad.unknown.injection.component.ReceiverComponent;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.schedulers.Schedulers;


/**
 * Created by armin on 4/11/17.
 */

public class InstantiateMatchReceiver extends BroadcastReceiver{

    @Inject
    public DataManager dataManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(Services.LOG,"InstantiateMatchReceiver is triggered now...");

        ReceiverComponent component = DaggerReceiverComponent.builder()
                .applicationComponent(((UnknownApplication) context.getApplicationContext()).getComponent())
                .build();
        component.inject(this);

        dataManager.readMatchesLocalObservable().subscribeOn(Schedulers.io())
                .subscribe(new Observer<Match>() {
                    @Override
                    public void onCompleted() {

                        Log.d(Services.LOG,"readMatches -> onCompleted");

                        dataManager.removeConsumedMatchInstances();


                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.d(Services.LOG,"readMatches -> onError: " + e.getMessage());

                    }

                    @Override
                    public void onNext(Match match) {

                        Log.d(Services.LOG,"readMatches -> onNext: " + match.toString());
                        List<MatchInstance> matchInstances = new Select().from(MatchInstance.class).where(MatchInstance_Table.type.eq(match.getType())).queryList();
                        if (matchInstances.isEmpty()) {
                            for (int i = 0; i < match.getNumberOfCreate(); i++) {
                                MatchInstance matchInstance = new MatchInstance(match);
                                matchInstance.save();
                            }
                        }

                    }
                });



    }

}
