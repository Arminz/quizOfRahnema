package com.example.emad.unknown.data.model;

import com.example.emad.unknown.data.local.entities.ExtraGame1;
import com.example.emad.unknown.data.local.entities.ExtraGame2;

/**
 * Created by emad on 4/18/17.
 */

public class GameTitle {

    private String type;
    private long id;
    int time;
    int numOfQuestions;
    int difficulty;
    int rewardCoins;
    int rewardGems;

    public GameTitle(ExtraGame1 extraGame1) {
        type = "extraGame1";
        this.id = extraGame1.getId();
    }

    public GameTitle(ExtraGame2 extraGame2) {
        type = "extraGame2";
        this.id = extraGame2.getId();

    }

    public long getId(){
        return id;
    }

    public String getType() {
        return type;
    }
}
