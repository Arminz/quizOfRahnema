package com.example.emad.unknown.ui.normalQuize;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.ui.base.MvpView;
import com.example.emad.unknown.ui.common.QuestionViewInterface;
import com.example.emad.unknown.ui.common.ResultViewInterface;
import com.example.emad.unknown.util.MatchSituation;

/**
 * Created by emad on 4/11/17.
 */

public interface NormalQuizMvpView extends MvpView, QuestionViewInterface, ResultViewInterface {

    void refreshQuestionView(Question question, int number,boolean hasGem);

    void refreshUserDetailView();

    void explodeHeart();

    void showResult(MatchSituation matchSituation,boolean victory);

}
