package com.example.emad.unknown.ui.firstSpecialQuiz;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.ResultData;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.common.QuestionView;
import com.example.emad.unknown.ui.common.ResultView;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.util.MatchSituation;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.sackcentury.shinebuttonlib.ShineButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by emad on 4/24/17.
 */

public class FirstQuizFragment extends BaseFragment implements FirstQuizMvpView {

    public static final String TAG = "FirstQuizFragment";

    @BindView(R.id.cards_container_type_one)
    SwipePlaceHolderView mViewHolder;

    @BindView(R.id.progressBar_quiz_one)
    ProgressBar progressBar;

    @BindView(R.id.star_one)
    ShineButton starOne;

    @BindView(R.id.star_two)
    ShineButton starTwo;

    @BindView(R.id.star_three)
    ShineButton starThree;

    @Inject
    FirstQuizMvpPresenter<FirstQuizMvpView> mPresenter;

//    public static FirstQuizFragment newInstance() {
//        Bundle args = new Bundle();
//        FirstQuizFragment fragment = new FirstQuizFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first_quiz, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        long quizId = getArguments().getLong("QUIZ_ID");

        mPresenter.startQuiz(quizId);

        mViewHolder.disableTouchSwipe();

        starOne.setEnabled(false);
        starTwo.setEnabled(false);
        starThree.setEnabled(false);

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onQuestionAnswered(Question question, int answer) {
        mPresenter.questionAnswered(question, answer);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mViewHolder.doSwipe(false);
                mPresenter.getQuestion();
            }
        }, 1200);
    }

    @Override
    public void onUseGem() {
        mPresenter.onUseGem();

    }

    @Override
    public void refreshTimeView(int sec, int max) {
        progressBar.setMax(max*50);
        //progressBar.setProgress(sec);
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", sec*50, 0);
        animation.setDuration(sec*1000);
        animation.start();
    }

    @Override
    public void refreshQuestionView(Question question, int number,boolean hasGem) {
        mViewHolder.addView(new QuestionView(question, this, getContext(), number, getActivity(),hasGem));
    }

    @Override
    public void showResult(MatchSituation matchSituation) {
//        increaseXP(gainXp);
//        increaseGem(gainGem);
        mViewHolder.doSwipe(false);
        mViewHolder.addView(new ResultView(matchSituation,this,"زمین بازی سه ستاره"));
    }

    @Override
    public void increaseXP(int deltaXP) {
        QuizActivity activity = (QuizActivity) getActivity();
        activity.increaseXP(deltaXP);
    }

    @Override
    public void increaseCoin(int deltaCoin) {
        QuizActivity activity = (QuizActivity) getActivity();
        activity.increaseCoin(deltaCoin);
    }

    @Override
    public void increaseGem(int deltaGem) {
        QuizActivity activity = (QuizActivity) getActivity();
        activity.increaseGem(deltaGem);

    }

    @Override
    public void setPhase(int phase) {
        if (phase == 1){
            starOne.callOnClick();
        }else if (phase == 2){
            starTwo.callOnClick();
        }else if (phase == 3){
            starThree.callOnClick();
        }
    }


    @Override
    public void onCloseClick() {
        ((QuizActivity)getActivity()).navigateToMain();
    }
}
