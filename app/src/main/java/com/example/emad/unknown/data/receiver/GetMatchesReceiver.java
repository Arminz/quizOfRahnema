package com.example.emad.unknown.data.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.UnknownApplication;
import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.injection.component.DaggerReceiverComponent;
import com.example.emad.unknown.injection.component.ReceiverComponent;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by armin on 5/9/17.
 */

public class GetMatchesReceiver extends BroadcastReceiver {

    @Inject
    DataManager dataManager;

    @Override
    public void onReceive(final Context context, Intent intent) {

        Log.d(AppConstants.Log.RECEIVER, "GetMatchesReceiver triggered now...");

        ReceiverComponent component = DaggerReceiverComponent.builder()
                .applicationComponent(((UnknownApplication) context.getApplicationContext()).getComponent())
                .build();
        component.inject(this);

        if (NetworkUtil.isNetworkConnected(context)) {
            UserAuth userAuth = new UserAuth();
            userAuth.setAndroidId(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            userAuth.setUsername(dataManager.getUsername());
            userAuth.setPassword(dataManager.getPassword());
            userAuth.setRefreshToken(dataManager.getRefreshToken());
            userAuth.setAccessToken(dataManager.getAccessToken());
            dataManager.getMatchesRemoteObservable(userAuth).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Match>() {
                        @Override
                        public void onCompleted() {
                            Log.d(AppConstants.Log.RETROFIT, "getMatches onCompleted.");

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            Log.d(AppConstants.Log.RETROFIT, "getMatches onError : " + e.toString());

                        }

                        @Override
                        public void onNext(Match match) {
                            saveMatch(match);


                        }
                    });
        }
    }

    void saveMatch(Match match)
    {
        Gson gson = new Gson();
        String json = gson.toJson(match);
//                            Toast.makeText(context, "getMatches : " + json, Toast.LENGTH_SHORT).show();
        Log.d(AppConstants.Log.RETROFIT, "getMatches onNext : " + json);
        dataManager.writeMatchLocalObservable(match).subscribeOn(Schedulers.io()).subscribe(new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d(AppConstants.Log.DBFlow,"write match complete");
//                                    Intent intent = new Intent(context,InstantiateMatchReceiver.class);
//                                    context.sendBroadcast(intent);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(AppConstants.Log.DBFlow,"write match ");

            }

            @Override
            public void onNext(Boolean aBoolean) {
                Log.d(AppConstants.Log.DBFlow,"write match onNext:" + aBoolean);

            }
        });

    }
}
