package com.example.emad.unknown.ui.base;

/**
 * Created by emad on 4/4/17.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

}