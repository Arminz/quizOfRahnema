package com.example.emad.unknown.ui.matchesInstancesList;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.main.MainActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emad on 4/17/17.
 */

public class MatchesInstancesListFragment extends BaseFragment implements MatchesInstancesListMvpView {


    public static final String TAG = "MatchesInstancesListFragment";

    @BindView(R.id.matches_instances_list_Recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private MatchesInstancesListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Inject
    MatchesInstancesListMvpPresenter mPresenter;

    public static MatchesInstancesListFragment newInstance() {
        Bundle args = new Bundle();
        MatchesInstancesListFragment fragment = new MatchesInstancesListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    ArrayList<MatchInstance> matchInstances;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matches_instances_list, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        mPresenter.updateMatchInstancesList();

        layoutManager = new LinearLayoutManager(getContext());
         matchInstances = new ArrayList<>();
//        matchInstances.add(new MatchInstance());
        adapter = new MatchesInstancesListAdapter(matchInstances,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.updateMatchInstancesList();
            }

        });

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void requestExtraGame(long id ,int quizType) {
        mPresenter.tryToStartQuiz(id, quizType);

    }

    @Override
    public void startExtraGame(long id, int quizType) {

        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.navigateToQuizActivity(quizType, id);

    }

    @Override
    public void forbiddenMatchClicked(View view) {
        YoYo.with(Techniques.Swing)
                .duration(500)
                .repeat(1)
                .playOn(view);

    }

    @Override
    public void setMatchesList(ArrayList<MatchInstance> matchesInstances) {
        adapter.setMatches(matchesInstances);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void refreshEnd() {
        if (swipeRefreshLayout.isRefreshing()){
            new Handler().postDelayed(new Runnable() {
                @Override public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, 2000);

        }
    }

    @Override
    public void notEnoughGold()
    {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.notEnoughGold();
    }

    public void updateList(){
        mPresenter.updateMatchInstancesList();
    }



}