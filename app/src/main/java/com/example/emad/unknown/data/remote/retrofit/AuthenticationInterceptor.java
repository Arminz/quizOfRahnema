package com.example.emad.unknown.data.remote.retrofit;

import android.util.Log;

import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.util.AppConstants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by armin on 2/4/17.
 */
public class AuthenticationInterceptor implements Interceptor {
    private UserAuth userAuth;

    public AuthenticationInterceptor(){}

    public void setUserAuth(UserAuth userAuth){
        this.userAuth = userAuth;
    }

    @Override
    public Response intercept(final Chain chain) throws IOException {

        final Request original = chain.request();
        final Request.Builder[] builder = {original.newBuilder()};

        if (userAuth.getAndroidId() != null)
                builder[0].header("androidId", userAuth.getAndroidId());
        if (userAuth.getUsername() != null && userAuth.getPassword() != null){
                builder[0].header("username", userAuth.getUsername());
                builder[0].header("password", userAuth.getPassword());
        }

        if (userAuth.isTryToUpdateToken() && (userAuth.getAccessToken() == null || userAuth.getAccessToken().isEmpty()) && userAuth.getType().equals(AppConstants.GRANT_TYPE.ACCESS_TOKEN))
            return expiredAccessToken(chain,builder[0]);
        if (userAuth.isTryToUpdateToken() && (userAuth.getRefreshToken() == null || userAuth.getRefreshToken().isEmpty()) && userAuth.getType().equals(AppConstants.GRANT_TYPE.REFRESH_TOKEN))
            return expiredRefreshToken(chain,builder[0]);
        builder[0].header("accept" , "application/json");
        Log.d(AppConstants.Log.RETROFIT,"userAuth : " + userAuth.toString());
        if (userAuth.getType().equals(AppConstants.GRANT_TYPE.ACCESS_TOKEN))
            builder[0].header("Authorization","Bearer " + userAuth.getAccessToken());
        else
            builder[0].header("Authorization", AppConstants.AUTH_KEY);


        Request request = builder[0].build();
        final Response[] response = {chain.proceed(request)};

        if (!userAuth.isTryToUpdateToken() || response[0].code() != 401)
        {
            return response[0];
        }
        else
        {
            Log.d(AppConstants.Log.RETROFIT,"401 with grantType : " + userAuth.getType());
            if (userAuth.getType().equals(AppConstants.GRANT_TYPE.ACCESS_TOKEN)) {
                return expiredAccessToken(chain,builder[0]);
            }
            else if (userAuth.getType().equals(AppConstants.GRANT_TYPE.REFRESH_TOKEN)){
                return expiredRefreshToken(chain,builder[0]);
            }
            else if (userAuth.getType().equals(AppConstants.GRANT_TYPE.PASSWORD))
            {
                Log.d(AppConstants.Log.RETROFIT, "the password is not existed");
                return response[0];
//                throw new RuntimeException("the password has been expired");
            }
            else
            {
                throw new RuntimeException("invalid grantType in userAuth unauthorized");
            }


        }

//        builder[0] = original.newBuilder().header("authorization",token).
//                method(original.method(),original.body());



    }

    private Response expiredRefreshToken(Chain chain, Request.Builder builder) throws IOException {
        Log.d(AppConstants.Log.RETROFIT, "refreshToken has been expired");
        userAuth.setType(AppConstants.GRANT_TYPE.PASSWORD);
        AuthResponse authResponse = updateRefreshToken();
        userAuth.setRefreshToken(authResponse.getRefresh_token());
        userAuth.setAccessToken(authResponse.getAccess_token());
        userAuth.setType(AppConstants.GRANT_TYPE.ACCESS_TOKEN);
        builder.header("Authorization", "Bearer " + userAuth.getAccessToken());
        return chain.proceed(builder.build());
    }

    private Response expiredAccessToken(Chain chain,Request.Builder builder) throws IOException {
        Log.d(AppConstants.Log.RETROFIT, "accessToken has been expired");
        if (userAuth.getRefreshToken() == null || userAuth.getRefreshToken().isEmpty())
            return expiredRefreshToken(chain,builder);
        userAuth.setType(AppConstants.GRANT_TYPE.REFRESH_TOKEN);
        String accessToken = updateAccessToken();
        userAuth.setAccessToken(accessToken);
        userAuth.setType(AppConstants.GRANT_TYPE.ACCESS_TOKEN);
        builder.header("Authorization", "Bearer " + userAuth.getAccessToken());
        return chain.proceed(builder.build());

    }
    private String updateAccessToken() throws IOException {
        Log.d(AppConstants.Log.RETROFIT, "going to update accessToken...");
        userAuth.setType(AppConstants.GRANT_TYPE.REFRESH_TOKEN);
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<AuthResponse> updateAccessToken = api.updateAccessToken(AppConstants.GRANT_TYPE.REFRESH_TOKEN,userAuth.getRefreshToken());
        retrofit2.Response<AuthResponse> authResponse = updateAccessToken.execute();
        userAuth.setType(AppConstants.GRANT_TYPE.ACCESS_TOKEN);
        return authResponse.body().getAccess_token();
    }

    private AuthResponse updateRefreshToken() throws IOException {
        Log.d(AppConstants.Log.RETROFIT, "going to update refreshToken...");
        userAuth.setType(AppConstants.GRANT_TYPE.PASSWORD);
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<AuthResponse> updateAccessToken = api.updateRefreshToken(AppConstants.GRANT_TYPE.PASSWORD,userAuth.getUsername(),userAuth.getPassword());
        retrofit2.Response<AuthResponse> authResponse = updateAccessToken.execute();
        userAuth.setType(AppConstants.GRANT_TYPE.REFRESH_TOKEN);
        return authResponse.body();
//        updateAccessToken.enqueue(new Callback<AuthResponse>() {
//            @Override
//            public void onResponse(Call<AuthResponse> call, retrofit2.Response<AuthResponse> response) {
//                try {
//                    Log.d(AppConstants.Log.RETROFIT, "refreshToken has been fetched : " + ((response.body() != null) ? response.body().toString() : "nullResponseBody"));
//                    callBack.doSth();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }            }
//
//            @Override
//            public void onFailure(Call<AuthResponse> call, Throwable t) {
//
//            }
//        });
    }

}