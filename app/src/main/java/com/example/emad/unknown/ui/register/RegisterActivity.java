package com.example.emad.unknown.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.example.emad.unknown.R;
import com.example.emad.unknown.ui.base.BaseActivity;
import com.example.emad.unknown.ui.main.MainActivity;
import com.example.emad.unknown.ui.signIn.SignInFragment;
import com.example.emad.unknown.ui.signUp.SignUpFragment;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by emad on 5/3/17.
 */

public class RegisterActivity extends BaseActivity implements RegisterMvpView {

    @Inject
    RegisterMvpPresenter<RegisterMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        if (mPresenter.isSignedUp()){
            navigateToMain();
        }

        navigateToSignIn();

    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(SignUpFragment.TAG);
        if (fragment != null) {
            navigateToSignIn();
            onFragmentDetached(SignUpFragment.TAG);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onFragmentAttached() {
        super.onFragmentAttached();
        Log.d("size_frag", "size:"+getSupportFragmentManager().getFragments().size());
    }

    @Override
    public void onFragmentDetached(String tag) {
        super.onFragmentDetached(tag);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.nothing,R.anim.slide_out_left_anim)
                    .disallowAddToBackStack()
                    .remove(fragment)
                    .commitNow();
        }

    }

    @Override
    public void navigateToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToSignIn() {
        onFragmentDetached(SignUpFragment.TAG);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(SignInFragment.TAG);
        if (fragment == null) {
            getSupportFragmentManager()
                    .beginTransaction()
//                .setCustomAnimations(R.anim.slide_in_left_anim,R.anim.slide_out_right_anim)
                    .replace(R.id.register_view_holder, SignInFragment.newInstance(), SignInFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void navigateToSignUp() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left_anim,R.anim.nothing)
                .add(R.id.register_view_holder, SignUpFragment.newInstance(), SignUpFragment.TAG)
                .commit();
    }
}
