package com.example.emad.unknown.util;

import com.example.emad.unknown.data.local.entities.MatchInstance;

import java.util.ArrayList;

/**
 * Created by armin on 5/23/17.
 */

public class MatchSituation {

    private int correctAnswers;
    private int wrongAnswers;
    private MatchInstance matchInstance;
    private ArrayList<Integer> phases;
    private int currentPhase;

    private int gemOne = 0;
    private int xpOne = 0;
    private int coinOne = 0;

    private int phaseGem = 0;
    private int phaseXp = 0;
    private int phaseCoin = 0;



    public MatchSituation(MatchInstance matchInstance) {
        this.correctAnswers = 0;
        this.wrongAnswers = 0;
        this.matchInstance = matchInstance;
        phases = MatchPhasesConstants.getPhases(matchInstance);
        currentPhase = 0;


    }

    public MatchSituation(int coin) {
        phaseCoin = coin;
    }

    public HitResponse hitCorrect()
    {
        HitResponse hitResponse = new HitResponse();
        hitResponse.setXpReward(matchInstance.getXpReward());
        correctAnswers ++;
        if (matchInstance.getType() == 2 || matchInstance.getType() == 1) {
            xpOne += matchInstance.getXpReward();
            hitResponse.setXpReward(matchInstance.getXpReward());
        }
        if (currentPhase + 1 < phases.size() && correctAnswers >= phases.get(currentPhase)) {
            hitResponse.setPhaseUpgraded(true);
            if (matchInstance.getType() == 2)
            {
                phaseXp += phases.get(currentPhase) * matchInstance.getXpReward();
                phaseGem += matchInstance.getGemReward();
                phaseCoin += matchInstance.getCoinReward();

                hitResponse.setPhaseXpReward(phases.get(currentPhase) * matchInstance.getXpReward());
                hitResponse.setPhaseGemReward(matchInstance.getGemReward());
                hitResponse.setPhaseCoinReward(matchInstance.getCoinReward());
            }
            else if (matchInstance.getType() == 1)
            {
                phaseXp += phases.get(currentPhase) * matchInstance.getXpReward();
                phaseGem += matchInstance.getGemReward();
                phaseCoin += matchInstance.getCoinReward();

                hitResponse.setPhaseXpReward(phases.get(currentPhase) * matchInstance.getXpReward());
                hitResponse.setPhaseCoinReward(matchInstance.getCoinReward());
                hitResponse.setPhaseGemReward(matchInstance.getGemReward());


            }
            else if (matchInstance.getType() == 0)
            {
                phaseCoin += matchInstance.getCoinReward();
                hitResponse.setPhaseCoinReward(phaseCoin);

            }
            else
                throw new RuntimeException("matchInstance illegal type: " + matchInstance.getType());
            currentPhase++;

        }
        return hitResponse;
    }
    public HitResponse hitWrong()
    {
        HitResponse hitResponse = new HitResponse();

        wrongAnswers ++;

        return hitResponse;
    }




    public void finish()
    {
        if (matchInstance.getType() == 2)
        {

            phaseGem += Dice.randomInt(currentPhase);

        }


    }

    public int getCurrentPhaseHitCorrectAnswers(){
        if (currentPhase == 0)
            return correctAnswers;
        return correctAnswers - phases.get(currentPhase - 1);
    }

    public int getCurrentPhaseTotalCorrectAnswers(){
        if (currentPhase == 0)
            return phases.get(currentPhase);
        return phases.get(currentPhase ) - phases.get(currentPhase - 1);
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public int getWrongAnswers() {
        return wrongAnswers;
    }

    public MatchInstance getMatchInstance() {
        return matchInstance;
    }

    public ArrayList<Integer> getPhases() {
        return phases;
    }

    public int getCurrentPhase() {
        return currentPhase;
    }

    public int getGemOne() {
        return gemOne;
    }

    public int getXpOne() {
        return xpOne;
    }

    public int getCoinOne() {
        return coinOne;
    }

    public int getPhaseGem() {
        return phaseGem;
    }

    public int getPhaseXp() {
        return phaseXp;
    }

    public int getPhaseCoin() {
        return phaseCoin;
    }

    public int getPassedQuestions()
    {
        return correctAnswers + wrongAnswers;
    }
}
