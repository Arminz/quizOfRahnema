package com.example.emad.unknown.data.remote.api;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by armin on 5/2/17.
 */

public interface API {

    @POST("/user/register")
    Call<UserDetails> registerUser();

    @POST("/user/sync-detail")
    Call<UserDetails> syncDetails(@Body UserDeltaDetails deltaDetails);

    @POST("/question/get/as")
    Call<QuestionsWrapper> getQuestions(@Body QuestionsSituationWrapper questionsSituationWrapper);

    @POST("/match/get")
    Call<ArrayList<Match>> getMatches();



    @POST("/oauth/token")
    Call<AuthResponse> updateRefreshToken(@Query("grant_type") String grantType,
                                          @Query("username") String username, @Query("password") String password);//update refreshToken

    @POST("/oauth/token")
    Call<AuthResponse> updateAccessToken(@Query("grant_type") String grantType,
                                          @Query("refresh_token") String refresh_token);//update accessToken

//    @POST("/question/get")
//    Call<ArrayList<QuestionModel>> getQuestions();

}
