package com.example.emad.unknown.data.remote.request;

/**
 * Created by armin on 5/2/17.
 */

public class UserIdWrapper<T> {
    private T id;
    public void setId(T id){this.id = id;}
    public T getId(){return id;}
}
