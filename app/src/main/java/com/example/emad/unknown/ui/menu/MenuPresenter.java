package com.example.emad.unknown.ui.menu;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/17/17.
 */

public class MenuPresenter<V extends MenuMvpView> extends BasePresenter<V>
        implements MenuMvpPresenter<V>{

    @Inject
    public MenuPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void tryToStartNormalQuiz() {
        if (20 > getDataManager().countUnseenQuestions()) {
            if (NetworkUtil.isNetworkConnected(getContext()) || true) {
                UserAuth userAuth = new UserAuth();
                userAuth.setAndroidId(Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                userAuth.setUsername(getDataManager().getUsername());
                userAuth.setPassword(getDataManager().getPassword());
                userAuth.setRefreshToken(getDataManager().getRefreshToken());
                userAuth.setAccessToken(getDataManager().getAccessToken());

                getDataManager().syncQuestionsRemoteObservable(getDataManager().getQuestionsSituation(), userAuth).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<QuestionsWrapper>() {
                            @Override
                            public void onCompleted() {
                                Log.d(AppConstants.Log.RETROFIT, "getQuestions onCompleted register");
                                getDataManager().deleteSeenQuestions();

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                Log.d(AppConstants.Log.RETROFIT, "getQuestions onError : " + e.toString());
                                Toast.makeText(getContext(), "امکان بارگزاری بازی جدید به دلیل نبود سوال وجود ندارد.", Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onNext(QuestionsWrapper questionsWrapper) {
                                Gson gson = new Gson();
                                String json = gson.toJson(questionsWrapper);
                                Log.d(AppConstants.Log.RETROFIT, "getQuestions onNext : " + json);
                                getDataManager().writeQuestionLocalObservable(questionsWrapper.getQuestions()).subscribeOn(Schedulers.io()).subscribe(new Observer<Boolean>() {
                                    @Override
                                    public void onCompleted() {
                                        Log.d(AppConstants.Log.DBFlow, "write new questions complete");
                                        getMvpView().showNormalQuiz();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d(AppConstants.Log.DBFlow, "write new questions error : " + e.getMessage());
                                        Toast.makeText(getContext(), "امکان بارگزاری بازی جدید به دلیل نبود سوال وجود ندارد.", Toast.LENGTH_SHORT).show();

                                    }

                                    @Override
                                    public void onNext(Boolean aBoolean) {
                                        Log.d(AppConstants.Log.DBFlow, "write new questions onNext:" + aBoolean);

                                    }
                                });

                            }
                        });
            } else {
                Toast.makeText(getContext(), "اتصال اینترنت خود را برای دریافت سوال جدید بررسی کنید.", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            getMvpView().showNormalQuiz();
        }

    }
}
