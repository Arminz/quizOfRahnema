package com.example.emad.unknown.ui.matchesList;

import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/17/17.
 */

public interface MatchesListMvpPresenter extends MvpPresenter<MatchesListMvpView>{

    void updateMatchesList();

}