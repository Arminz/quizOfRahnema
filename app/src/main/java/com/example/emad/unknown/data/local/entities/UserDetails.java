package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.google.gson.Gson;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by emad on 5/2/17.
 */
@Table(database = DataBase.class)
public class UserDetails extends BaseModel{

    @Column
    @PrimaryKey
    private int primaryKey;

    @Column
    private long gems;

    @Column
    private long coin;

    @Column
    private long xp;

    @Column
    private long level;

    public UserDetails(long gems, long coins, long xp, long level) {
        this.gems = gems;
        this.coin = coins;
        this.xp = xp;
        this.level = level;
    }

    public UserDetails() {
        this.gems = 0;
        this.coin = 0;
        this.xp = 0;
        this.level = 0;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    public long getGems() {
        return gems;
    }

    public void setGems(long gems) {
        this.gems = gems;
    }

    public long getCoin() {
        return coin;
    }

    public void setCoin(long coin) {
        this.coin = coin;
    }

    public long getXp() {
        return xp;
    }

    public void setXp(long xp) {
        this.xp = xp;
    }

    public long getLevel() {
        return level;
    }

    public void setLevel(long level) {
        this.level = level;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
