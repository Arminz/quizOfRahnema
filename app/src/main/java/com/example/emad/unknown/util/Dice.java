package com.example.emad.unknown.util;

import android.util.Log;

import java.util.Random;

/**
 * Created by armin on 4/12/17.
 */

public class Dice{
    public static int getRandomNumber(double p)
    {
        int t = 0;
        while (getRandomBoolean(p)) {
            Log.d("tagDice", String.valueOf(t));
            t++;
        }
        return t;

    }
    public static boolean getRandomBoolean(double p)
    {
        Random random = new Random();
        double diceP = random.nextDouble()%1;
        Log.d("tagDice" , p + " : " + diceP);
        return (diceP < p);

    }
    public static int randomInt(int x)
    {
        Random random = new Random();
        return random.nextInt() % x;
    }
}
