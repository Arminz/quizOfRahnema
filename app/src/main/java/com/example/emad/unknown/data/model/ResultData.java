package com.example.emad.unknown.data.model;

/**
 * Created by emad on 5/25/17.
 */

public class ResultData {

    int perQCoin;

    int rewardCoin;

    int perQGem;

    int rewardGem;

    int perQXp;

    int rewardXp;

    public ResultData(int perQCoin, int rewardCoin, int perQGem, int rewardGem, int perQXp, int rewardXp) {
        this.perQCoin = perQCoin;
        this.rewardCoin = rewardCoin;
        this.perQGem = perQGem;
        this.rewardGem = rewardGem;
        this.perQXp = perQXp;
        this.rewardXp = rewardXp;
    }

    public int getPerQCoin() {
        return perQCoin;
    }

    public void setPerQCoin(int perQCoin) {
        this.perQCoin = perQCoin;
    }

    public int getPerQGem() {
        return perQGem;
    }

    public void setPerQGem(int perQGem) {
        this.perQGem = perQGem;
    }

    public int getRewardCoin() {
        return rewardCoin;
    }

    public void setRewardCoin(int rewardCoin) {
        this.rewardCoin = rewardCoin;
    }

    public int getRewardGem() {
        return rewardGem;
    }

    public void setRewardGem(int rewardGem) {
        this.rewardGem = rewardGem;
    }

    public int getPerQXp() {
        return perQXp;
    }

    public void setPerQXp(int perQXp) {
        this.perQXp = perQXp;
    }

    public int getRewardXp() {
        return rewardXp;
    }

    public void setRewardXp(int rewardXp) {
        this.rewardXp = rewardXp;
    }
}
