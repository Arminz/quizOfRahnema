package com.example.emad.unknown.data.manager.remote.sockets;

import android.util.Log;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.QuestionAction;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.APIError;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.ErrorUtils;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.util.AppConstants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by armin on 5/3/17.
 */

public class SyncQuestionsSocket implements Observable.OnSubscribe<QuestionsWrapper> {

    private UserAuth userAuth;

    private QuestionsSituationWrapper questionsSituationWrapper;

    private SyncQuestionsSocket(QuestionsSituationWrapper questionsSituationWrapper, UserAuth userAuth){
        this.questionsSituationWrapper = questionsSituationWrapper;
        this.userAuth = userAuth;
    }

    @Override
    public void call(final Subscriber<? super QuestionsWrapper> subscriber) {
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<QuestionsWrapper> questionsWrapperCall = api.getQuestions(questionsSituationWrapper);
        questionsWrapperCall.enqueue(new Callback<QuestionsWrapper>() {
            @Override
            public void onResponse(Call<QuestionsWrapper> call, Response<QuestionsWrapper> response) {
                if (response.isSuccessful()) {
                    subscriber.onNext(response.body());
                    subscriber.onCompleted();
                }else{
                    APIError error = ErrorUtils.parseError(response);
                    if (error.message() != null) {
                        Log.d(AppConstants.Log.RETROFIT, error.message());
                        subscriber.onError(new Throwable(error.message()));
                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionsWrapper> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"error connecting to server...");
                subscriber.onError(t);

            }
        });
    }

    public static Observable<QuestionsWrapper> getObservable(QuestionsSituationWrapper questionsSituationWrapper, UserAuth userAuth) {
        return Observable.create(new SyncQuestionsSocket(questionsSituationWrapper,userAuth));
    }


}
