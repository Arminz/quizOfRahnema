package com.example.emad.unknown.data.model;

import com.example.emad.unknown.util.AppConstants;
import com.google.gson.Gson;

/**
 * Created by emad on 5/2/17.
 */

public class UserAuth {

    private String username, password;

    private String type = AppConstants.GRANT_TYPE.ACCESS_TOKEN;

    private String token,refreshToken,accessToken;

    private String androidId;

    private boolean tryToUpdateToken = true;


    public boolean isTryToUpdateToken() {
        return tryToUpdateToken;
    }

    public void setTryToUpdateToken(boolean tryToUpdateToken) {
        this.tryToUpdateToken = tryToUpdateToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    //    String androidId;
//
//    String username;
//
//    String password;
//
//    String email;
//
//    String phoneNumber;
//
//    String firstName;
//
//    String lastName;
//
//    String token;
//
//    public UserAuth() {}
//
    public String getAndroidId() {
        return androidId;
    }
//
    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getToken() {
//        return token;
//    }
//
//    public void setToken(String token){this.token = token;}
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String getType() {
        return type;
    }



}
