package com.example.emad.unknown.data.manager;

import com.example.emad.unknown.data.local.entities.ExtraGame1;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import rx.Observable;
import rx.Subscriber;


/**
 * Created by armin on 4/30/17.
 */

public class ExtraGame1sObservable implements Observable.OnSubscribe<ExtraGame1> {

    public static Observable<ExtraGame1> getInstance(){return Observable.create(new ExtraGame1sObservable());}


    @Override
    public void call(Subscriber<? super ExtraGame1> subscriber) {
        try {
            List<ExtraGame1> extraGame1s = SQLite.select().from(ExtraGame1.class).queryList();
            for (int i = 0; i < extraGame1s.size(); i++)
                subscriber.onNext(extraGame1s.get(i));
            subscriber.onCompleted();
        }catch (Exception exception){
            subscriber.onError(exception);
        }

    }
}
