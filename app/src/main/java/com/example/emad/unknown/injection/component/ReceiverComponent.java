package com.example.emad.unknown.injection.component;

import com.example.emad.unknown.data.receiver.GetMatchesReceiver;
import com.example.emad.unknown.data.receiver.GetQuestionsReceiver;
import com.example.emad.unknown.data.receiver.InstantiateMatchReceiver;
import com.example.emad.unknown.data.receiver.UpdateTokensReceiver;
import com.example.emad.unknown.injection.PreReceiver;
import com.example.emad.unknown.injection.module.ReceiverModule;


import dagger.Component;

/**
 * Created by emad on 5/9/17.
 */

@PreReceiver
@Component(dependencies = ApplicationComponent.class, modules = ReceiverModule.class)
public interface ReceiverComponent {

    void inject(InstantiateMatchReceiver instantiateMatchReceiver);

    void inject(GetMatchesReceiver getMatchesReceiver);

    void inject(GetQuestionsReceiver getQuestionsReceiver);

    void inject(UpdateTokensReceiver updateTokensReceiver);

}
