package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.google.gson.Gson;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by emad on 5/2/17.
 */

@Table(database = DataBase.class)
public class UserDeltaDetails extends BaseModel{

    @Column
    @PrimaryKey
    private int primaryKey;

    @Column(defaultValue = "0")
    private long gem;

    @Column(defaultValue = "0")
    private long coin;

    @Column(defaultValue = "0")
    private long xp;

    @Column(defaultValue = "0")
    private long level;

    public UserDeltaDetails(long gems, long coins, long xp, long level) {
        this.gem = gems;
        this.coin = coins;
        this.xp = xp;
        this.level = level;
    }

    public UserDeltaDetails(){
        this.gem = 0;
        this.coin = 0;
        this.xp = 0;
        this.level = 0;
    }


    public long getCoin() {
        return coin;
    }

    public void setCoin(long coin) {
        this.coin = coin;
    }

    public long getXp() {
        return xp;
    }

    public void setXp(long xp) {
        this.xp = xp;
    }

    public long getLevel() {
        return level;
    }

    public void setLevel(long level) {
        this.level = level;
    }

    public void refresh() {
        gem = 0;
        xp = 0;
        level = 0;
        coin = 0;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public long getGem() {
        return gem;
    }

    public void setGem(long gem) {
        this.gem = gem;
    }

    public void setPrimaryKey(int primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public void incrementXp(int xpReward) {
        xp += xpReward;
    }
}
