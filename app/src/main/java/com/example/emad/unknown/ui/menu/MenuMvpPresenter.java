package com.example.emad.unknown.ui.menu;

import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/17/17.
 */

public interface MenuMvpPresenter<V extends MenuMvpView> extends MvpPresenter<V> {

    void tryToStartNormalQuiz();
}
