package com.example.emad.unknown.ui.main;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.receiver.GetMatchesReceiver;
import com.example.emad.unknown.data.receiver.GetQuestionsReceiver;
import com.example.emad.unknown.data.receiver.InstantiateMatchReceiver;
import com.example.emad.unknown.data.receiver.UpdateTokensReceiver;
import com.example.emad.unknown.data.services.AlarmManagersHandler;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/6/17.
 */

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

//    @Override
//    public int getUserCoins() {
//        return 0;
//    }
//
//    @Override
//    public void setUserCoins(int x) {}
//
//    @Override
//    public int getUserGems() {
//        return 0;
//    }
//
//    @Override
//    public void setUserGems(int x) {}
//
//    @Override
//    public int getUserRank() {return 0;}
//
//    @Override
//    public int getUserXp(){return 0;}
//
//    @Override
//    public void setUserXp(int xp) {//
//         }
//
//    @Override
//    public void setUserRank(int x) {

//    }

    @Override
    public void saveUserDetails(UserDetails userDetails) {
        getDataManager().writeUserDetailsObservable(userDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d(AppConstants.Log.ALGORITHM_FLOW,"saveUserDetails -> onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(AppConstants.Log.ALGORITHM_FLOW,"saveUserDetails -> onError : " + e.getMessage());
            }

            @Override
            public void onNext(Boolean aBoolean) {
                Log.d(AppConstants.Log.ALGORITHM_FLOW,"saveUserDetails -> onNext : " + aBoolean);
            }

        });
    }

    @Override
    public boolean isSignedUp(){
        return getDataManager().isSignedUp();
    }

    @Override
    public void refreshUserDetails() {

        Log.d(AppConstants.Log.ALGORITHM_FLOW,"going to refresh user info...");
        if (NetworkUtil.isNetworkConnected(getContext())) {
            final UserAuth userAuth = new UserAuth();
            userAuth.setAndroidId(Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
            userAuth.setUsername(getDataManager().getUsername());
            userAuth.setPassword(getDataManager().getPassword());
            userAuth.setRefreshToken(getDataManager().getRefreshToken());
            userAuth.setAccessToken(getDataManager().getAccessToken());

            userAuth.setToken(getDataManager().getTokenType()+" "+getDataManager().getAccessToken());

            getDataManager().getUserDeltaDetails()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<UserDeltaDetails>() {
                @Override
                public void onCompleted() {
                    Log.d(AppConstants.Log.DBFlow,"readUserDeltaDetails -> onCompleted");
                }
                @Override
                public void onError(Throwable e) {
                    Log.d(AppConstants.Log.DBFlow,"readUserDeltaDetails -> onError : " + e.getMessage());
                }
                @Override
                public void onNext(UserDeltaDetails userDeltaDetails) {
                    Log.d(AppConstants.Log.DBFlow,"readUserDeltaDetails -> onNext : " + userDeltaDetails.toString());
                    getDataManager().syncUserDetailsRemoteObservable(userDeltaDetails,userAuth).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Observer<UserDetails>() {
                                @Override
                                public void onCompleted() {
                                    Log.d(AppConstants.Log.RETROFIT,"syncUserDetailsRemoteObservable completed.");

                                }
                                @Override
                                public void onError(Throwable e) {
                                    Log.d(AppConstants.Log.RETROFIT,e.getMessage());
                                    e.printStackTrace();
                                    refreshDetailView();
                                }
                                @Override
                                public void onNext(final UserDetails userDetails) {
                                    if (userDetails == null)
                                        return;
                                    getDataManager().writeUserDetailsObservable(userDetails).subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new Observer<Boolean>() {
                                        @Override
                                        public void onCompleted() {
                                            refreshDetailView();
                                            Log.d(AppConstants.Log.DBFlow,"saveUserDetails -> onCompleted.: ");
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Log.d(AppConstants.Log.DBFlow,"saveUserDetails -> onError : " + e.getMessage());
                                        }

                                        @Override
                                        public void onNext(Boolean aBoolean) {
                                            Log.d(AppConstants.Log.DBFlow,"saveUserDetails -> onNext.");

                                        }
                                    });

                                }
                            });

                }
            });
        }
        else {
            refreshDetailView();
        }

    }

    @Override
    public void signUp() {
        Intent intent = new Intent(getContext(), AlarmManagersHandler.class);
        getContext().sendBroadcast(intent);
    }

    @Override
    public void getMatches() {
        Intent intent = new Intent(getContext(), GetMatchesReceiver.class);
        getContext().sendBroadcast(intent);

    }

    @Override
    public void getQuestions() {
        Intent intent = new Intent(getContext(), GetQuestionsReceiver.class);
        getContext().sendBroadcast(intent);
    }


    @Override
    public void updateTokens() {
        Intent intent = new Intent(getContext(), UpdateTokensReceiver.class);
        getContext().sendBroadcast(intent);
    }

    @Override
    public void refreshDetailView(){
        getDataManager().getUserDeltaDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UserDeltaDetails>() {
                    @Override
                    public void call(final UserDeltaDetails userDeltaDetails) {
                        getDataManager().readUserDetailsObservable()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<UserDetails>() {
                                    @Override
                                    public void call(UserDetails userDetails) {
                                        getMvpView().refreshUserDetailsView(new UserDetails(
                                                userDeltaDetails.getGem()+userDetails.getGems(),
                                                userDeltaDetails.getCoin()+userDetails.getCoin(),
                                                userDeltaDetails.getXp()+userDetails.getXp(),
                                                userDeltaDetails.getLevel()+userDetails.getLevel()));
                                    }
                                });
                    }
                });
    }

    @Override
    public void instantiateMatches() {
        Intent intent1 = new Intent(getContext(), InstantiateMatchReceiver.class);
        getContext().sendBroadcast(intent1);
    }
}
