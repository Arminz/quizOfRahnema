package com.example.emad.unknown.ui.matchesList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.main.MainActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by emad on 4/17/17.
 */

public class MatchesListFragment extends BaseFragment implements MatchesListMvpView {


    public static final String TAG = "MatchesListFragment";

    @BindView(R.id.return_menu_btn)
    Button returnBtn;

    @BindView(R.id.matches_list_Recycler)
    RecyclerView recyclerView;

    private MatchesListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Inject
    MatchesListMvpPresenter mPresenter;

    public static MatchesListFragment newInstance() {
        Bundle args = new Bundle();
        MatchesListFragment fragment = new MatchesListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_matches_list, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        mPresenter.updateMatchesList();

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MatchesListAdapter(new ArrayList<Match>(),this);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }


//
//    @Override
//    public void requestExtraGame(long id ,int quizType) {
//        MainActivity mainActivity = (MainActivity) getActivity();
//        mainActivity.navigateToQuizActivity(quizType, id);
//    }

    @Override
    public void setMatchesList(ArrayList<Match> matches) {

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter.setMatches(matches);
        adapter.notifyDataSetChanged();

    }

}