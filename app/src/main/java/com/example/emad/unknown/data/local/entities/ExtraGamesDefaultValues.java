package com.example.emad.unknown.data.local.entities;

/**
 * Created by armin on 4/9/17.
 */

public interface ExtraGamesDefaultValues {
    interface Type1 {
        String INIT_TIME = "30";
        String PENALTY_TIME = "20";
        String PENALTY_COST = "20";
        String REWARD_TIME = "30";
        String REWARD_COIN = "20";
        String START_COST = "40";
    }
    interface Type2 {
        String START_COST = "30";
        String TIME_BOUND_PER_QUESTION = "30";
        String REWARD_COIN = "20";
    }
}
