package com.example.emad.unknown.data.local.prefs;


import java.sql.Timestamp;

/**
 * Created by emad on 4/6/17.
 */

public interface PreferencesHelper {

    void setSignedUp(boolean situation);

    boolean isSignedUp();

    Timestamp getUserLastSyncDetails();

    void setUserLastSyncDetails(Timestamp syncTime);

    Timestamp getUserLastSyncMatches();

    void setUserLastSyncMatches(Timestamp syncTime);

    Timestamp getUserLastSyncQuestions();

    void setUserLastSyncQuestions(Timestamp syncTime);

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getAccessToken();

    void setAccessToken(String accessToken);

    String getRefreshToken();

    void setRefreshToken(String refreshToken);

    String getTokenType();

    void setTokenType(String tokenType);
}
