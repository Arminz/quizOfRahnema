package com.example.emad.unknown.ui.matchesInstancesList;

import android.app.Dialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.util.DialogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emad on 4/17/17.
 */

public class MatchesInstancesListAdapter extends RecyclerView.Adapter<MatchesInstancesListAdapter.ViewHolder> {
    private List<MatchInstance> matchesInstances;
    private MatchesInstancesListMvpView gamesListMvpView;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView typeTV;
        TextView enterCoastTV;
        TextView rewardXp;
        TextView rewardCoinTV;
        CardView cardView;

        public ViewHolder(final View view) {
            super(view);
            this.cardView = (CardView) view.findViewById(R.id.card_view);
            this.typeTV = (TextView) cardView.findViewById(R.id.type);
            this.enterCoastTV = (TextView) cardView.findViewById(R.id.enter_coast);
//            this.rewardGemTV = (TextView) cardView.findViewById(R.id.reward_gem);
            this.rewardXp = (TextView) cardView.findViewById(R.id.reward_xp);
            this.rewardCoinTV = (TextView) cardView.findViewById(R.id.reward_coin);

        }
    }


    public MatchesInstancesListAdapter(ArrayList<MatchInstance> matchesInstances, MatchesInstancesListMvpView gamesListMvpView) {
        this.matchesInstances = matchesInstances;
        this.gamesListMvpView = gamesListMvpView;
    }

    public void setMatches(ArrayList<MatchInstance> matchesInstances){
        this.matchesInstances = matchesInstances;
    }

    public void addMatchInstance(MatchInstance matchInstance){
        this.matchesInstances.add(matchInstance);
    }

    @Override
    public MatchesInstancesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_instance_row,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        MatchInstance matchInstance = matchesInstances.get(position);
        if (matchInstance.isConsumed())
            holder.typeTV.setText("مصرف شده");
        else if (matchInstance.getType() == 1){
            holder.typeTV.setText("بازی سه ستاره");
        }
        else{
            holder.typeTV.setText("بازی فازی");
        }
        holder.rewardCoinTV.setText(""+matchInstance.getCoinReward());
//        holder.rewardGemTV.setText("الماس جایزه: "+ matchInstance.getGemReward());
        holder.enterCoastTV.setText(""+matchInstance.getEnterCoin());
        holder.rewardXp.setText("" + matchInstance.getXpReward());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MatchInstance matchInstance = matchesInstances.get(holder.getAdapterPosition());
                if (matchInstance.isConsumed()) {
                    gamesListMvpView.forbiddenMatchClicked(v);
                    return;
                }
                final Dialog dialog = DialogFactory.showEnterDialog(v.getContext());
                Button cancel = (Button) dialog.findViewById(R.id.cancel);
                Button ok = (Button) dialog.findViewById(R.id.ok);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(holder.getAdapterPosition()%2 == 0)
                            gamesListMvpView.requestExtraGame(matchInstance.getId(), matchInstance.getType());
                        else
                            gamesListMvpView.requestExtraGame(matchInstance.getId(), matchInstance.getType());
                        dialog.dismiss();
                    }
                });


            }
        });

    }

    @Override
    public int getItemCount() {
        return matchesInstances.size();
    }
}

