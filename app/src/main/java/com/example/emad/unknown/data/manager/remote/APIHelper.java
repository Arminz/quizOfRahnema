package com.example.emad.unknown.data.manager.remote;

import com.example.emad.unknown.data.local.entities.Match;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.manager.remote.sockets.AuthSocket;
import com.example.emad.unknown.data.manager.remote.sockets.MatchesRemoteSocket;
import com.example.emad.unknown.data.manager.remote.sockets.RegisterUserSocket;
import com.example.emad.unknown.data.manager.remote.sockets.SyncDetailsSocket;
import com.example.emad.unknown.data.manager.remote.sockets.SyncQuestionsSocket;
import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.QuestionAction;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by armin on 5/2/17.
 */

@Singleton
public class APIHelper implements APIHelperInterface {

    @Inject
    public APIHelper() {
    }

    @Override
    public Observable<UserDetails> registerUserObservable(UserAuth userAuth){
        return RegisterUserSocket.getObservable(userAuth);
    }

    @Override
    public Observable<UserDetails> syncUserDetailsRemoteObservable(UserDeltaDetails userDeltaDetails, UserAuth userAuth) {
        return SyncDetailsSocket.getObservable(userDeltaDetails,userAuth);
    }

    @Override
    public Observable<QuestionsWrapper> syncQuestionsRemoteObservable(QuestionsSituationWrapper questionsSituationWrapper, UserAuth userAuth) {
        return SyncQuestionsSocket.getObservable(questionsSituationWrapper,userAuth);
    }

    @Override
    public Observable<Match> getMatchesRemoteObservable(UserAuth userAuth) {
        return MatchesRemoteSocket.getObservable(userAuth);
    }

    @Override
    public Observable<AuthResponse> authUser(UserAuth userAuth) {
        return AuthSocket.getObservable(userAuth);
    }


//    @Override
//    public Observable<List<QuestionModel>>  getQuestionsRemoteObservable(UserAuth userAuth) {
//        return SyncQuestionsSocket.getObservable(userAuth);
//    }


}
