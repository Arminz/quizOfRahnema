package com.example.emad.unknown.ui.secondSpecialQuiz;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.MatchInstance;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.ResultData;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.common.QuestionView;
import com.example.emad.unknown.ui.common.ResultView;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.util.ConvertToArabicNumber;
import com.example.emad.unknown.util.MatchSituation;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.sackcentury.shinebuttonlib.ShineButton;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emad on 4/30/17.
 */

public class SecondQuizFragment extends BaseFragment implements SecondQuizMvpView {

    public static final String TAG = "SecondQuizFragment";

    @BindView(R.id.cards_container_type_two)
    SwipePlaceHolderView mViewHolder;

    @BindView(R.id.progressBar_quiz_two)
    ProgressBar progressBar;

    @BindView(R.id.progress_phase)
    RoundCornerProgressBar progressPhase;

    @BindView(R.id.progress_text)
    TextView progressText;

    @BindView(R.id.star_phase)
    ShineButton shineStar;

    @BindView(R.id.phase_text)
    TextView phaseText;

    @Inject
    SecondQuizMvpPresenter<SecondQuizMvpView> mPresenter;

    private MatchInstance matchInstance;

//    public static SecondQuizFragment newInstance() {
//        Bundle args = new Bundle();
//        SecondQuizFragment fragment = new SecondQuizFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_quiz, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        long quizId = getArguments().getLong("QUIZ_ID");

        mPresenter.onAttach(this);

        mPresenter.startQuiz(quizId);

        mViewHolder.disableTouchSwipe();

        shineStar.setEnabled(false);
        shineStar.setChecked(false);

        return view;
    }

    @Override
    public void onQuestionAnswered(Question question, int answer) {
        mPresenter.questionAnswered(question, answer);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mViewHolder.doSwipe(false);
                mPresenter.getQuestion();
            }
        }, 1200);

    }

    @Override
    public void onUseGem() {
        mPresenter.onUseGem();

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void refreshTimeView(int sec, int max) {
        progressBar.setMax(max*1000);
        //progressBar.setProgress(sec);
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", sec*1000, 0);
        animation.setDuration(sec*1000);
        animation.start();
    }

    @Override
    public void refreshQuestionView(Question question, int number,boolean hasGem) {
        mViewHolder.addView(new QuestionView(question, this, getContext(), number, getActivity(),hasGem));
    }

    @Override
    public void showResult(MatchSituation matchSituation) {
//        increaseXp(gainXp);
//        increaseGem(gainGem);
        mViewHolder.doSwipe(false);
        mViewHolder.addView(new ResultView(matchSituation,this,"زمین بازی فازی"));
    }

    @Override
    public void increaseXp(int deltaXp) {
        QuizActivity activity = (QuizActivity) getActivity();
        activity.increaseXP(deltaXp);

    }

    @Override
    public void increaseGem(int deltaGem) {
        QuizActivity activity = (QuizActivity) getActivity();
        activity.increaseGem(deltaGem);

    }

    @Override
    public void increaseCoin(int deltaCoin) {
        QuizActivity activity = (QuizActivity) getActivity();
        activity.increaseCoin(deltaCoin);

    }

    @Override
    public void refreshPhaseView(int correct, int max, int phase, boolean upgrade) {
        progressPhase.setMax(max);
        progressPhase.setProgress(correct);
        progressText.setText(ConvertToArabicNumber.convert(correct+"/"+max));
        if (upgrade){
            shineStar.callOnClick();
        }
        phaseText.setText(ConvertToArabicNumber.convert(""+phase));
    }


    @Override
    public void onCloseClick() {
        ((QuizActivity)getActivity()).navigateToMain();
    }
}
