package com.example.emad.unknown.ui.signUp;

import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 5/3/17.
 */

public interface SignUpMvpPresenter<V extends SignUpMvpView> extends MvpPresenter<V> {

    void registerUser(String username, String password);

}
