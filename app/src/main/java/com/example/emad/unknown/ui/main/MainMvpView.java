package com.example.emad.unknown.ui.main;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.ui.base.MvpView;

/**
 * Created by emad on 4/6/17.
 */

public interface MainMvpView extends MvpView {

    void refreshUserDetailsView(UserDetails userDetails);

    void notEnoughGold();

}
