package com.example.emad.unknown.ui.firstSpecialQuiz;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.ui.base.MvpView;
import com.example.emad.unknown.ui.common.QuestionViewInterface;
import com.example.emad.unknown.ui.common.ResultViewInterface;
import com.example.emad.unknown.util.MatchSituation;

/**
 * Created by emad on 4/24/17.
 */

public interface FirstQuizMvpView extends MvpView, QuestionViewInterface, ResultViewInterface {

    void refreshTimeView(int sec, int max);

    void  refreshQuestionView(Question question, int number,boolean hasGem);

    void showResult(MatchSituation matchSituation);

    void increaseXP(int deltaXP);

    void increaseCoin(int deltaCoin);

    void increaseGem(int deltaGem);

    void setPhase(int phase);
}
