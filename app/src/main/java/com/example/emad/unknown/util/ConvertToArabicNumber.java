package com.example.emad.unknown.util;

/**
 * Created by emad on 5/24/17.
 */

public class ConvertToArabicNumber {
    public static String convert(String string){
        return string.replaceAll("1","١").replaceAll("0","۰")
                .replaceAll("3","۳").replaceAll("2","۲")
                .replaceAll("5","۵").replaceAll("4","۴")
                .replaceAll("7","۷").replaceAll("6","۶")
                .replaceAll("9","۹").replaceAll("8","۸");
    }
}
