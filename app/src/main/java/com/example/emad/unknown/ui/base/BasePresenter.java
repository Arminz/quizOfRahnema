package com.example.emad.unknown.ui.base;

import android.content.Context;

import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;

import javax.inject.Inject;

/**
 * Created by emad on 4/4/17.
 */

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private V mMvpView;

    private final DataManager mDataManager;

    private final Context context;

    @Inject
    public BasePresenter(DataManager dataManager, @ApplicationContext Context context) {
        this.mDataManager = dataManager;
        this.context = context;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public Context getContext(){
        return context;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

}
