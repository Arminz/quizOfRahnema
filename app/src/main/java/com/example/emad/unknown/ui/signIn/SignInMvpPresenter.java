package com.example.emad.unknown.ui.signIn;

import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 5/3/17.
 */

public interface SignInMvpPresenter<V extends SignInMvpView> extends MvpPresenter<V> {

    void signIn(String username, String password);

}
