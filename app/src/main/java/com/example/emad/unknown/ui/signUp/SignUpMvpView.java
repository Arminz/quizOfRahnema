package com.example.emad.unknown.ui.signUp;

import com.example.emad.unknown.ui.base.MvpView;

/**
 * Created by emad on 5/3/17.
 */

public interface SignUpMvpView extends MvpView {

    void navigateToMain();

    void disableSignUpBtn();

    void enableSignUpBtn();

    void showProgressBar();

    void hideProgressBar();

    void signUpError(String type);

}
