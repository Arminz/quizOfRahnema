package com.example.emad.unknown.injection.module;

import android.app.Activity;
import android.content.Context;

import com.example.emad.unknown.injection.ActivityContext;
import com.example.emad.unknown.injection.PerActivity;
import com.example.emad.unknown.ui.firstSpecialQuiz.FirstQuizMvpPresenter;
import com.example.emad.unknown.ui.firstSpecialQuiz.FirstQuizMvpView;
import com.example.emad.unknown.ui.firstSpecialQuiz.FirstQuizPresenter;
import com.example.emad.unknown.ui.matchesInstancesList.MatchesInstancesListMvpPresenter;
import com.example.emad.unknown.ui.matchesInstancesList.MatchesInstancesListPresenter;
import com.example.emad.unknown.ui.matchesList.MatchesListMvpPresenter;
import com.example.emad.unknown.ui.matchesList.MatchesListPresenter;
import com.example.emad.unknown.ui.main.MainMvpPresenter;
import com.example.emad.unknown.ui.main.MainMvpView;
import com.example.emad.unknown.ui.main.MainPresenter;
import com.example.emad.unknown.ui.menu.MenuMvpPresenter;
import com.example.emad.unknown.ui.menu.MenuMvpView;
import com.example.emad.unknown.ui.menu.MenuPresenter;
import com.example.emad.unknown.ui.normalQuize.NormalQuizMvpPresenter;
import com.example.emad.unknown.ui.normalQuize.NormalQuizMvpView;
import com.example.emad.unknown.ui.normalQuize.NormalQuizPresenter;
import com.example.emad.unknown.ui.quiz.QuizMvpPresenter;
import com.example.emad.unknown.ui.quiz.QuizMvpView;
import com.example.emad.unknown.ui.quiz.QuizPresenter;
import com.example.emad.unknown.ui.register.RegisterMvpPresenter;
import com.example.emad.unknown.ui.register.RegisterMvpView;
import com.example.emad.unknown.ui.register.RegisterPresenter;
import com.example.emad.unknown.ui.secondSpecialQuiz.SecondQuizMvpPresenter;
import com.example.emad.unknown.ui.secondSpecialQuiz.SecondQuizMvpView;
import com.example.emad.unknown.ui.secondSpecialQuiz.SecondQuizPresenter;
import com.example.emad.unknown.ui.signIn.SignInMvpPresenter;
import com.example.emad.unknown.ui.signIn.SignInMvpView;
import com.example.emad.unknown.ui.signIn.SignInPresenter;
import com.example.emad.unknown.ui.signUp.SignUpMvpPresenter;
import com.example.emad.unknown.ui.signUp.SignUpMvpView;
import com.example.emad.unknown.ui.signUp.SignUpPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emad on 4/6/17.
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity){
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    // use annotations @Provides and @PerActivity before presenters providers!!!

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    NormalQuizMvpPresenter<NormalQuizMvpView> provideNormalQuizPresenter(NormalQuizPresenter<NormalQuizMvpView> presenter) {
        return presenter;
    }

    @Provides
    MenuMvpPresenter<MenuMvpView> provideMenuPresenter(MenuPresenter<MenuMvpView> presenter) {
        return presenter;
    }

    @Provides
    MatchesListMvpPresenter provideMatchesListPresenter(MatchesListPresenter presenter) {
        return presenter;
    }

    @Provides
    MatchesInstancesListMvpPresenter provideMatchesInstancesListPresenter(MatchesInstancesListPresenter presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    QuizMvpPresenter<QuizMvpView> provideQuizPresenter(QuizPresenter<QuizMvpView> presenter) {
        return presenter;
    }

    @Provides
    FirstQuizMvpPresenter<FirstQuizMvpView> provideFirstQUizPresenter(FirstQuizPresenter<FirstQuizMvpView> presenter) {
        return presenter;
    }

    @Provides
    SecondQuizMvpPresenter<SecondQuizMvpView> provideSecondQUizPresenter(SecondQuizPresenter<SecondQuizMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegisterMvpPresenter<RegisterMvpView> provideRegisterActivityPresenter(RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    SignInMvpPresenter<SignInMvpView> provideSignInPresenter(SignInPresenter<SignInMvpView> presenter) {
        return presenter;
    }

    @Provides
    SignUpMvpPresenter<SignUpMvpView> provideSignUpPresenter(SignUpPresenter<SignUpMvpView> presenter) {
        return presenter;
    }

}
