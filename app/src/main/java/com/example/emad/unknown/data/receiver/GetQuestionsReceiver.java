package com.example.emad.unknown.data.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.example.emad.unknown.UnknownApplication;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.data.model.OptionsWrapperModel;
import com.example.emad.unknown.data.model.QuestionModel;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.injection.component.DaggerReceiverComponent;
import com.example.emad.unknown.injection.component.ReceiverComponent;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.NetworkUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by armin on 5/9/17.
 */

public class GetQuestionsReceiver extends BroadcastReceiver {

    @Inject
    DataManager dataManager;

    @Override
    public void onReceive(final Context context, Intent intent) {

        Log.d(AppConstants.Log.RECEIVER, "GetQuestionsReceiver triggered now...");

        ReceiverComponent component = DaggerReceiverComponent.builder()
                .applicationComponent(((UnknownApplication) context.getApplicationContext()).getComponent())
                .build();
        component.inject(this);

        if (NetworkUtil.isNetworkConnected(context)) {
            UserAuth userAuth = new UserAuth();
            userAuth.setAndroidId(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            userAuth.setUsername(dataManager.getUsername());
            userAuth.setPassword(dataManager.getPassword());
            userAuth.setRefreshToken(dataManager.getRefreshToken());
            userAuth.setAccessToken(dataManager.getAccessToken());

            dataManager.syncQuestionsRemoteObservable(dataManager.getQuestionsSituation(),userAuth).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<QuestionsWrapper>() {
                        @Override
                        public void onCompleted() {
                            Log.d(AppConstants.Log.RETROFIT, "getQuestions onCompleted register");
                            dataManager.deleteSeenQuestions();

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            Log.d(AppConstants.Log.RETROFIT, "getQuestions onError : " + e.toString());
//                            makeFakeQuestions();

                        }

                        @Override
                        public void onNext(QuestionsWrapper questionsWrapper) {
                            saveQuestions(questionsWrapper);

                        }
                    });
        }
//        else
//            makeFakeQuestions();


    }


    void saveQuestions(QuestionsWrapper questionsWrapper)
    {
        Gson gson = new Gson();
        String json = gson.toJson(questionsWrapper);
//                            Toast.makeText(context, "new questions : " + json, Toast.LENGTH_SHORT).show();
        Log.d(AppConstants.Log.RETROFIT, "getQuestions onNext : " + json);
        dataManager.writeQuestionLocalObservable(questionsWrapper.getQuestions()).subscribeOn(Schedulers.io()).subscribe(new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d(AppConstants.Log.DBFlow, "write new questions complete");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(AppConstants.Log.DBFlow, "write new questions error : " + e.getMessage());

            }

            @Override
            public void onNext(Boolean aBoolean) {
                Log.d(AppConstants.Log.DBFlow, "write new questions onNext:" + aBoolean);

            }
        });

    }
}
