package com.example.emad.unknown.data.services;

import android.content.Context;
import android.util.Log;

import com.example.emad.unknown.data.local.Unknown;
import com.example.emad.unknown.data.local.entities.ExtraGame1;
import com.example.emad.unknown.data.local.entities.ExtraGame2;
import com.example.emad.unknown.ui.Alarm.NotificationHandler;
import com.example.emad.unknown.util.Dice;



import static com.example.emad.unknown.data.services.Services.MakeExtraGameProperties.PROBABILITIES.*;



/**
 * Created by armin on 4/12/17.
 */

public class MakeExtraGame {
    private static MakeExtraGame instance = null;
    public static MakeExtraGame getInstance()
    {
        if (instance == null)
            instance = new MakeExtraGame();
        return instance;
    }
    private MakeExtraGame (){}

    public void makeExtraGames(Context context)
    {
        Log.d(Unknown.Log, " going to make extraGames");


        int randomNumber1 = Dice.getRandomNumber(EXTRA_GAME1_CHANCE);
        makeMultipleExtraGame1(randomNumber1);


        int randomNumber2 = Dice.getRandomNumber(EXTRA_GAME2_CHANCE);
        makeMultipleExtraGame2(randomNumber2);
        NotificationHandler.Notify(context,randomNumber1 + " extra game has been built. " + randomNumber2 + "extraGame2Number has been built.");

    }

    private void makeMultipleExtraGame1(int number)
    {

        Log.d(Unknown.Log, "going to create " + number + " extraGame1");
        for (int i = 0 ; i < number ; i ++)
            makeExtraGame1();

    }
    private void makeExtraGame1()
    {
        Log.d(Unknown.Log , "going to create extraGame1");
        ExtraGame1 extraGame1 = new ExtraGame1();
        extraGame1.save();
        Log.d(Unknown.Log , "extraGame1 has been saved.");

    }
    private void makeMultipleExtraGame2(int number)
    {
        Log.d(Unknown.Log, "going to create " + number + " extraGame2");
        for (int i = 0 ; i < number ; i ++)
            makeExtraGame2();

    }
    private void makeExtraGame2()
    {
        Log.d(Unknown.Log, "going to create extraGame2");
        ExtraGame2 extraGame2 = new ExtraGame2();
        extraGame2.save();
        Log.d(Unknown.Log, "extraGame2 has been saved.");

    }

}
