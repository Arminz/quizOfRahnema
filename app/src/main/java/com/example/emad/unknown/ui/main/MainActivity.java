package com.example.emad.unknown.ui.main;

import android.content.Intent;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.services.AlarmManagersHandler;
import com.example.emad.unknown.ui.base.BaseActivity;
import com.example.emad.unknown.ui.matchesInstancesList.MatchesInstancesListFragment;
import com.example.emad.unknown.ui.matchesList.MatchesListFragment;
import com.example.emad.unknown.ui.menu.MenuFragment;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.util.AppConstants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import devlight.io.library.ntb.NavigationTabBar;

public class MainActivity extends BaseActivity implements MainMvpView{

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.coinTextView)
    TextView coinTV;

    @BindView(R.id.gemTextView)
    TextView gemTV;

    @BindView(R.id.rankTextView)
    TextView rankTV;

    @BindView(R.id.xpTextView)
    TextView xpTV;

    @BindView(R.id.ntb)
    NavigationTabBar navigationTabBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);


//        DialogFactory.showAlertDialog(this).show();

//        if(!mPresenter.isSignedUp()){
//            mPresenter.signUp();
//        }

//        mPresenter.refreshUserDetails();
//
   //     mPresenter.getMatches();
        mPresenter.refreshUserDetails();

        mPresenter.getMatches();

        mPresenter.getQuestions();

        mPresenter.instantiateMatches();

//
//        mPresenter.getQuestions();



        viewPager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager()));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1){
                    ((ScreenSlidePagerAdapter)viewPager.getAdapter()).getCurrentFragment().updateList();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        AlarmManagersHandler alarmManagersHandler = AlarmManagersHandler.getInstance();
        alarmManagersHandler.startExtraGamesService(getBaseContext());

        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_home_black_48dp),
                        R.color.questionCardNonAccept
                ).title("")
                        .badgeTitle("NTB")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.listicon),
                        R.color.questionCardNonAccept
                ).title("")
                        .badgeTitle("with")
                        .build()
        );
        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager);
        navigationTabBar.setTitleMode(NavigationTabBar.TitleMode.ACTIVE);
        navigationTabBar.setBadgeGravity(NavigationTabBar.BadgeGravity.BOTTOM);
        navigationTabBar.setBadgePosition(NavigationTabBar.BadgePosition.CENTER);
        navigationTabBar.setIsBadged(true);
        navigationTabBar.setIsTinted(true);
        navigationTabBar.setIsSwiped(true);
        navigationTabBar.setBadgeSize(10);
        navigationTabBar.setTitleSize(10);
        navigationTabBar.setIconSizeFraction((float) 0.5);
    }


    @OnClick(R.id.questionsButton)
    public void syncQuestions()
    {
        mPresenter.getQuestions();
    }
    @OnClick(R.id.tokenButton)
    public void updateToken()
    {
        mPresenter.updateTokens();
    }
    @OnClick(R.id.matchButton)
    public void syncMach()
    {
        mPresenter.getMatches();
    }
    @OnClick(R.id.detailsButton)
    public void syncDet()
    {
        mPresenter.refreshUserDetails();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }


    @Override
    public void refreshUserDetailsView(UserDetails userDetails) {
        coinTV.setText("" + userDetails.getCoin());
        gemTV.setText("" + userDetails.getGems());
        rankTV.setText("" + userDetails.getLevel());
        xpTV.setText("" + userDetails.getXp());
    }

    @Override
    public void notEnoughGold() {
        YoYo.with(Techniques.Shake)
                .duration(500)
                .repeat(1)
                .playOn(coinTV);
    }


    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment fragment1 = fragmentManager.findFragmentByTag(MatchesListFragment.TAG);
        Fragment fragment2 = fragmentManager.findFragmentByTag(MatchesInstancesListFragment.TAG);

        if (fragment1 != null) {
            onFragmentDetached(MatchesListFragment.TAG);
            return;
        }
        if (fragment2 != null)
        {
            onFragmentDetached(MatchesInstancesListFragment.TAG);
            return;
        }
//        super.onBackPressed();
        moveTaskToBack(true);
    }

    @Override
    public void onFragmentAttached() {
        super.onFragmentAttached();
        Log.d("size_frag", "size:"+getSupportFragmentManager().getFragments().size());
    }
    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.refreshDetailView();
    }

    @Override
    public void onFragmentDetached(String tag) {
        super.onFragmentDetached(tag);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .remove(fragment)
                    .commitNow();
        }

    }
    public void navigateToQuizActivity(int quizType, long quizId){
        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra(AppConstants.QUIZ_TYPE, quizType);
        intent.putExtra(AppConstants.QUIZ_ID, quizId);
        startActivity(intent);
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        MatchesInstancesListFragment fragment2;

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return MenuFragment.newInstance();
            }
            else {
                fragment2 = MatchesInstancesListFragment.newInstance();
                return fragment2;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        public MatchesInstancesListFragment getCurrentFragment() {
            return fragment2;
        }
    }




}