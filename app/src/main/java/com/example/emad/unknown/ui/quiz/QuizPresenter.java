package com.example.emad.unknown.ui.quiz;

import android.content.Context;

import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.ApplicationContext;
import com.example.emad.unknown.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/17/17.
 */

public class QuizPresenter<V extends QuizMvpView> extends BasePresenter<V>
        implements QuizMvpPresenter<V> {

    @Inject
    public QuizPresenter(DataManager dataManager, @ApplicationContext Context context) {
        super(dataManager, context);
    }

    @Override
    public void refreshDetails() {
        getDataManager().getUserDeltaDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UserDeltaDetails>() {
                    @Override
                    public void call(final UserDeltaDetails userDeltaDetails) {
                        getDataManager().readUserDetailsObservable()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<UserDetails>() {
                                    @Override
                                    public void call(UserDetails userDetails) {
                                        getMvpView().refreshUserDetailsView(new UserDetails(
                                                userDeltaDetails.getGem()+userDetails.getGems(),
                                                userDeltaDetails.getCoin()+userDetails.getCoin(),
                                                userDeltaDetails.getXp()+userDetails.getXp(),
                                                userDeltaDetails.getLevel()+userDetails.getLevel()));
                                    }
                                });
                    }
                });
    }
}
