package com.example.emad.unknown.data.remote.retrofit;

import android.text.TextUtils;
import android.util.Log;


import com.example.emad.unknown.data.model.AccessToken;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.util.AppConstants;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by armin on 2/4/17.
 */
public class ServiceGenerator {

    private static final String API_BASE_URL = AppConstants.URL;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit retrofit;

    public static Retrofit retrofit() {
        OkHttpClient client = httpClient.build();
        return builder.client(client).build();

    }


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

//    public static <S> S createService(Class<S> serviceClass) {
//        Log.d(AppConstants.Log.RETROFIT,"creates service null null");
//        return createService(serviceClass, null, null);
//    }


    public static <S> S createService(Class<S> serviceClass , UserAuth userAuth){

        if (userAuth == null)
            throw new RuntimeException("userAuth cannot be null.");

        Log.d(AppConstants.Log.RETROFIT,"creates service with userAuth : " + userAuth.toString());

        AuthenticationInterceptor authenticationInterceptor = new AuthenticationInterceptor();
//        if (userAuth.getToken() == null && userAuth.getUsername() != null && userAuth.getPassword() != null) {
//            String authToken = Credentials.basic(userAuth.getUsername(), userAuth.getPassword());
//            userAuth.setToken(authToken);
//        }
        authenticationInterceptor.setUserAuth(userAuth);
        httpClient.addInterceptor(authenticationInterceptor);



        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        builder.client(httpClient.build());

        return builder.build().create(serviceClass);

    }
    public static <S> S createService(Class<S> serviceClass , AccessToken accessToken){

        if (accessToken == null)
            throw new RuntimeException("accessToken cannot be null.");

        Log.d(AppConstants.Log.RETROFIT,"creates service with accessToken : " + accessToken.toString());

//        builder

        AuthenticationInterceptor authenticationInterceptor = new AuthenticationInterceptor();
//        if (userAuth.getToken() == null && userAuth.getUsername() != null && userAuth.getPassword() != null) {
//            String authToken = Credentials.basic(userAuth.getUsername(), userAuth.getPassword());
//            userAuth.setToken(authToken);
//        }
//        authenticationInterceptor.setUserAuth(userAuth);
        httpClient.addInterceptor(authenticationInterceptor);



        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        builder.client(httpClient.build());

        return builder.build().create(serviceClass);

    }
//    public static <S> S createService(
//            Class<S> serviceClass, String androidId) {
//        Log.d(AppConstants.Log.RETROFIT,"create service androidId: " + androidId);
//
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpClient.addInterceptor(logging);
//
//        if (!httpClient.interceptors().contains(interceptor)) {
//            httpClient.addInterceptor(interceptor);
//            AuthenticationInterceptor interceptor = new AuthenticationInterceptor();
//            interceptor.setAndroidId(androidId);
//        }
//        builder.client(httpClient.build());
//
//
//
//
//        return builder.build().create(serviceClass);
//    }
//    public static <S> S createService(
//            Class<S> serviceClass, String username, String password) {
//        Log.d(AppConstants.Log.RETROFIT,"create service user,pass: " + username + ":" + password);
//        if (!TextUtils.isEmpty(username)
//                && !TextUtils.isEmpty(password)) {
//            String authToken = Credentials.basic(username, password);
//            return createService(serviceClass, authToken);
//        }
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpClient.addInterceptor(logging);
//
//        AuthenticationInterceptor interceptor = new AuthenticationInterceptor();
////        interceptor.setAndroidId(");
//
//
//
//        return builder.build().create(serviceClass);
//    }

//    public static <S> S createService(
//            Class<S> serviceClass, final String authToken) {
//
//        Log.d(AppConstants.Log.RETROFIT,"create service auth : " + authToken);
//
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpClient.addInterceptor(logging);
//
//        if (!TextUtils.isEmpty(authToken)) {
//            AuthenticationInterceptor interceptor =
//                    new AuthenticationInterceptor();
//            interceptor.setAuthToken(authToken);
//
//            if (!httpClient.interceptors().contains(interceptor)) {
//                httpClient.addInterceptor(interceptor);
//
//
//                builder.client(httpClient.build());
//                return builder.build().create(serviceClass);
//            }
//        }
//
//        Log.d(AppConstants.Log.RETROFIT,"holly shit");
//        return builder.build().create(serviceClass);
//        //https://futurestud.io/tutorials/android-basic-authentication-with-retrofit//
//
//    }
}
