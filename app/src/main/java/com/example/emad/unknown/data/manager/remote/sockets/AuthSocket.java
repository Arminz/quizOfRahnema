package com.example.emad.unknown.data.manager.remote.sockets;

import android.util.Log;

import com.example.emad.unknown.data.model.AuthResponse;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.APIError;
import com.example.emad.unknown.data.remote.retrofit.ErrorHandlers.ErrorUtils;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.util.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by emad on 5/13/17.
 */

public class AuthSocket implements Observable.OnSubscribe<AuthResponse> {

    private UserAuth userAuth;

    public static Observable<AuthResponse> getObservable(UserAuth userAuth){return Observable.create(new AuthSocket(userAuth));}

    public AuthSocket(UserAuth userAuth) {
        this.userAuth = userAuth;
    }


    @Override
    public void call(final Subscriber<? super AuthResponse> subscriber) {
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<AuthResponse> responseCall = api.updateRefreshToken("password", userAuth.getUsername(), userAuth.getPassword());
        responseCall.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                Log.d("retrofit_url: ", call.request().url().toString());
                if (response.isSuccessful()) {
                    Log.d(AppConstants.Log.RETROFIT,response.raw().toString());
                    subscriber.onNext(response.body());
                    subscriber.onCompleted();

                }
                else {
                    APIError error = ErrorUtils.parseError(response);
                    Log.d(AppConstants.Log.RETROFIT,(error.message()!= null) ? error.message() : "");
                    if (userAuth.getType().equals(AppConstants.GRANT_TYPE.PASSWORD) && response.code() == 401)
                        subscriber.onError(new Throwable(AppConstants.SignErrors.WRONG_PASSWORD_USERNAME));
                    else
                        subscriber.onError(new Throwable(error.message()));
                }

            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"error connecting to server...");
                subscriber.onError(t);
            }
        });
    }
}
