package com.example.emad.unknown.data.local.entities;

import com.example.emad.unknown.data.local.DataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import static com.example.emad.unknown.data.local.entities.ExtraGamesDefaultValues.Type1.*;
/**
 * Created by armin on 4/9/17.
 */

@Table(database = DataBase.class)
public class ExtraGame1 extends BaseModel{

    @Column
    @PrimaryKey(autoincrement = true)
    private int id;

    @Column(defaultValue = INIT_TIME)
    private int initTime;

    @Column(defaultValue = PENALTY_TIME)
    private int penaltyTime;

    @Column(defaultValue = PENALTY_COST)
    private int penaltyCost;

    @Column(defaultValue = REWARD_TIME)
    private int rewardTime;

    @Column(defaultValue = REWARD_COIN)
    private int rewardCoin;

    @Column(defaultValue = START_COST)
    private int startCost;

    public int getId() {
        return id;
    }

    public int getInitTime() {
        return initTime;
    }

    public int getPenaltyTime() {
        return penaltyTime;
    }

    public int getPenaltyCost() {
        return penaltyCost;
    }

    public int getRewardTime() {
        return rewardTime;
    }

    public int getRewardCoin() {
        return rewardCoin;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setInitTime(int initTime) {
        this.initTime = initTime;
    }

    public void setPenaltyTime(int penaltyTime) {
        this.penaltyTime = penaltyTime;
    }

    public void setPenaltyCost(int penaltyCost) {
        this.penaltyCost = penaltyCost;
    }

    public void setRewardTime(int rewardTime) {
        this.rewardTime = rewardTime;
    }

    public void setRewardCoin(int rewardCoin) {
        this.rewardCoin = rewardCoin;
    }

    public void setStartCost(int startCost) {
        this.startCost = startCost;
    }

    public int getStartCost() {
        return startCost;
    }
}
