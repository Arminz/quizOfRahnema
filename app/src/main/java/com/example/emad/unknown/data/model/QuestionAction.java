package com.example.emad.unknown.data.model;

import android.support.annotation.Nullable;

import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.util.AppConstants;

/**
 * Created by armin on 5/3/17.
 */

public class QuestionAction {

    private long id;
    private int type;
    private Question question;

    public QuestionAction(long id, int type,@Nullable Question question) {
        this.id = id;
        this.type = type;
        this.question = question;
        if (type == AppConstants.QUESTION_ACTION_UPDATE && question == null)
            throw new RuntimeException("update questions null exception -> questions:null");
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
