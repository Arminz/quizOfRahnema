package com.example.emad.unknown.ui.matchesInstancesList;

import android.view.View;

import com.example.emad.unknown.ui.base.MvpPresenter;

/**
 * Created by emad on 4/17/17.
 */

public interface MatchesInstancesListMvpPresenter extends MvpPresenter<MatchesInstancesListMvpView>{

    void updateMatchInstancesList();

    void tryToStartQuiz( long id, int quizType);
}