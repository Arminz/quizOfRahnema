package com.example.emad.unknown.ui.signUp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.emad.unknown.R;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.register.RegisterActivity;
import com.example.emad.unknown.util.AppConstants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by emad on 5/3/17.
 */

public class SignUpFragment extends BaseFragment implements SignUpMvpView {

    public static final String TAG = "SignUpFragment";

    @BindView(R.id.sign_up_button)
    Button signUpBtn;

    @BindView(R.id.link_to_sign_in_text_view)
    Button linkToSignIn;

    @BindView(R.id.su_loading_progressbar)
    ProgressBar progressBar;

    @BindView(R.id.username_edit_text)
    EditText usernameET;

    @BindView(R.id.pass_edit_text)
    EditText passwordET;

    @Inject
    SignUpMvpPresenter<SignUpMvpView> mPresenter;

    public static SignUpFragment newInstance() {
        Bundle args = new Bundle();
        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.link_to_sign_in_text_view)
    void linkToSignInFunc(){
        ((RegisterActivity)getActivity()).navigateToSignIn();
    }

    @Override
    public void navigateToMain() {
        ((RegisterActivity)getActivity()).navigateToMain();
    }

    @Override
    public void disableSignUpBtn() {
        signUpBtn.setEnabled(false);
    }

    @Override
    public void enableSignUpBtn() {
        signUpBtn.setEnabled(true);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void signUpError(String type) {
        switch (type) {
            case AppConstants.SignErrors.usernameEmpty:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(usernameET);
                break;
            case AppConstants.SignErrors.passwordEmpty:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(passwordET);
                break;
            case AppConstants.SignErrors.NO_INTERNET_CONNECTION:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(signUpBtn);
                Toast.makeText(getContext(), "اتصال اینترنت خود را بررسی کنید.", Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.SignErrors.ERROR_CONNECTING_SERVER:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(signUpBtn);
                Toast.makeText(getContext(), "مشکل در ارتباط با سرور", Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.SignErrors.USED_USERNAME:
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(usernameET);
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .repeat(1)
                        .playOn(passwordET);
                Toast.makeText(getContext(), "نام کاربری قبلا ثبت نام شده است.", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getContext(),type,Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.sign_up_button)
    void registerUser(){
        if (checkUsernameEditText() && checkPassEditText())
            mPresenter.registerUser(usernameET.getText().toString(), passwordET.getText().toString());
    }

    boolean checkUsernameEditText(){
        if (usernameET.getText().toString().equals("")){
            usernameET.setError("please enter username");
            return false;
        }
        return true;
    }

    boolean checkPassEditText(){
        if (passwordET.getText().toString().equals("")){
            passwordET.setError("please enter password");
            return false;
        }
        return true;
    }

}
