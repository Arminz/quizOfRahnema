package com.example.emad.unknown;

import android.app.Application;

import com.example.emad.unknown.data.manager.DataManager;
import com.example.emad.unknown.injection.component.ApplicationComponent;
import com.example.emad.unknown.injection.component.DaggerApplicationComponent;
import com.example.emad.unknown.injection.module.ApplicationModule;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowLog;
import com.raizlabs.android.dbflow.config.FlowManager;

import javax.inject.Inject;

/**
 * Created by emad on 3/31/17.
 */

public class UnknownApplication extends Application {

    @Inject
    DataManager mDataManager;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        FlowManager.init(new FlowConfig.Builder(this).build());

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);

        FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);

    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
