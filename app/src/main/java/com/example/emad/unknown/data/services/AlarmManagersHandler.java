package com.example.emad.unknown.data.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.emad.unknown.data.receiver.GetMatchesReceiver;
import com.example.emad.unknown.data.receiver.GetQuestionsReceiver;
import com.example.emad.unknown.data.receiver.InstantiateMatchReceiver;
import com.example.emad.unknown.data.receiver.UpdateTokensReceiver;
import com.example.emad.unknown.injection.component.ApplicationComponent;
import com.example.emad.unknown.util.AppConstants;

import java.util.Calendar;

import static com.example.emad.unknown.data.services.Services.MakeExtraGameProperties.*;
import static com.example.emad.unknown.util.AppConstants.ReceiverRequestCodes.*;

/**
 * Created by armin on 4/11/17.
 */

public class AlarmManagersHandler {

    private static AlarmManagersHandler servicesHandler;
    private ApplicationComponent applicationComponent;



    public static AlarmManagersHandler getInstance(){

        if (servicesHandler == null)
            servicesHandler = new AlarmManagersHandler();
        return servicesHandler;
    }
    private AlarmManagersHandler(){

    }

    public void startExtraGamesService(Context context){


        Log.d(Services.LOG,"going to start extra game services.");

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, HOUR_OF_DAY);
        calendar.set(Calendar.MINUTE,MINUTE);
        calendar.set(Calendar.SECOND, SECOND);

        Intent intent = new Intent(context,InstantiateMatchReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, INSTANTIATE_MATCHES,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(SERVICE_TYPE,calendar.getTimeInMillis(), AppConstants.INTERVALS.INSTANTIATE_MATCH,pendingIntent);

        Intent intent2 = new Intent(context,GetQuestionsReceiver.class);
        PendingIntent pendingIntent2 = PendingIntent.getBroadcast(context, GET_QUESTIONS,intent2,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager2 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager2.setRepeating(SERVICE_TYPE,calendar.getTimeInMillis(),AppConstants.INTERVALS.SYNC_QUESTIONS,pendingIntent2);

        Intent intent3 = new Intent(context,GetMatchesReceiver.class);
        PendingIntent pendingIntent3 = PendingIntent.getBroadcast(context, GET_MATCHES,intent3,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager3 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager3.setRepeating(SERVICE_TYPE,calendar.getTimeInMillis(),AppConstants.INTERVALS.SYNC_MATCHES,pendingIntent3);

        Intent intent4 = new Intent(context,UpdateTokensReceiver.class);
        PendingIntent pendingIntent4 = PendingIntent.getBroadcast(context, UPDATE_TOKENS,intent4,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager4 = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager4.setRepeating(SERVICE_TYPE,calendar.getTimeInMillis(),AppConstants.INTERVALS.UPDATE_TOKEN,pendingIntent4);


//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,REQUEST_CODE, extraGameServiceIntent,FLAG);
//
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarmManager.setRepeating(SERVICE_TYPE,calendar.getTimeInMillis(),INTERVAL,pendingIntent);

        Log.d(Services.LOG,"alarmManagers are set now.");
    }
}
