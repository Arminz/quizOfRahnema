package com.example.emad.unknown.ui.normalQuize;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.Question;
import com.example.emad.unknown.data.model.ResultData;
import com.example.emad.unknown.ui.base.BaseFragment;
import com.example.emad.unknown.ui.common.QuestionView;
import com.example.emad.unknown.ui.common.ResultView;
import com.example.emad.unknown.ui.quiz.QuizActivity;
import com.example.emad.unknown.util.AppConstants;
import com.example.emad.unknown.util.MatchSituation;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.listeners.ItemRemovedListener;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tyrantgit.explosionfield.ExplosionField;

/**
 * Created by emad on 4/10/17.
 */

public class NormalQuizFragment extends BaseFragment implements NormalQuizMvpView {

    public static final String TAG = "NormalQuizFragment";

    @BindView(R.id.cards_container)
    SwipePlaceHolderView mViewHolder;

    @BindView(R.id.button2)
    Button newtBtn;

    @BindView(R.id.heart_one)
    View heartOne;

    @BindView(R.id.heart_two)
    View heartTwo;

    @BindView(R.id.heart_three)
    View heartThree;

    private ExplosionField mExplosionField;

    private int numOfHearts = 3;

    @Inject
    NormalQuizMvpPresenter<NormalQuizMvpView> mPresenter;

    public static NormalQuizFragment newInstance() {
        Bundle args = new Bundle();
        NormalQuizFragment fragment = new NormalQuizFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_normal_quiz, container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);

        mPresenter.startQuiz();

        mViewHolder.disableTouchSwipe();

        numOfHearts = 3;

        mExplosionField = ExplosionField.attach2Window(getActivity());

        return view;
    }

    @Override
    public void refreshQuestionView(Question question, int number,boolean hasGem) {
        newtBtn.setEnabled(false);
        mViewHolder.addView(new QuestionView(question, this, getContext(), number, getActivity(),hasGem));
//        mViewHolder.doSwipe(new QuestionView(question, this, getContext()),true);
    }

    @Override
    public void refreshUserDetailView() {
        ((QuizActivity) getActivity()).refreshUserDetails();
    }

    @Override
    public void explodeHeart() {
        if (numOfHearts == 3){
            mExplosionField.explode(heartThree);
            numOfHearts--;
        } else if (numOfHearts == 2){
            mExplosionField.explode(heartTwo);
            numOfHearts--;
        } else if (numOfHearts == 1){
            mExplosionField.explode(heartOne);
            mPresenter.finishQuiz();
            numOfHearts--;
        }
    }

    @Override
    public void showResult(MatchSituation matchSituation,boolean victory) {
        newtBtn.setEnabled(false);
        if (victory)
            mViewHolder.addView(new ResultView(matchSituation,this,"شما پیروز شدید!"));
        else
            mViewHolder.addView(new ResultView(matchSituation,this,"شما شکست خوردید !"));
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @OnClick(R.id.button2)
    public void nextQuestion(){

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mViewHolder.doSwipe(false);
                mPresenter.getNewQuestion();
            }
        }, 50);

    }

    @Override
    public void onQuestionAnswered(Question question, int answer) {

        newtBtn.setEnabled(true);

        Log.d(AppConstants.Log.QUIZ_FLOW, "onQuestionAnswered : " + answer);

        mPresenter.questionAnswered(question, answer);
    }

    @Override
    public void onUseGem() {

        mPresenter.onUseGem();

    }


    @Override
    public void onCloseClick() {
        ((QuizActivity)getActivity()).navigateToMain();
    }
}
