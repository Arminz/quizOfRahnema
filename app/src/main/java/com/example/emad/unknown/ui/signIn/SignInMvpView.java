package com.example.emad.unknown.ui.signIn;

import com.example.emad.unknown.ui.base.MvpView;

/**
 * Created by emad on 5/3/17.
 */

public interface SignInMvpView extends MvpView {

    void navigateToMain();

    void disableSignInBtn();

    void enableSignInBtn();

    void signInError(String type);
}
