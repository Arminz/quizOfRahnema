package com.example.emad.unknown.data.manager.remote.sockets;

import android.util.Log;

import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.model.QuestionsSituationWrapper;
import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.response.QuestionsWrapper;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.util.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by emad on 5/10/17.
 */

public class QuestionsAndDetailSocket implements Observable.OnSubscribe<QuestionsWrapper> {

    private UserAuth userAuth;
    private QuestionsSituationWrapper questionsSituationWrapper;

    public QuestionsAndDetailSocket(UserAuth userAuth, QuestionsSituationWrapper questionsSituationWrapper) {
        this.userAuth = userAuth;
        this.questionsSituationWrapper = questionsSituationWrapper;
    }

    @Override
    public void call(Subscriber<? super QuestionsWrapper> subscriber) {
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<QuestionsWrapper> syncDetails = api.getQuestions(questionsSituationWrapper);
        syncDetails.enqueue(new Callback<QuestionsWrapper>() {
            @Override
            public void onResponse(Call<QuestionsWrapper> call, Response<QuestionsWrapper> response) {

            }

            @Override
            public void onFailure(Call<QuestionsWrapper> call, Throwable t) {

            }
        });
    }
}
