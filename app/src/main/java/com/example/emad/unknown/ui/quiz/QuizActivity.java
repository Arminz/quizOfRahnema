package com.example.emad.unknown.ui.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emad.unknown.R;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.ui.base.BaseActivity;
import com.example.emad.unknown.ui.firstSpecialQuiz.FirstQuizFragment;
import com.example.emad.unknown.ui.main.MainActivity;
import com.example.emad.unknown.ui.normalQuize.NormalQuizFragment;
import com.example.emad.unknown.ui.secondSpecialQuiz.SecondQuizFragment;
import com.example.emad.unknown.util.AppConstants;

import java.security.Timestamp;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by emad on 4/17/17.
 */

public class QuizActivity extends BaseActivity implements QuizMvpView {

    @Inject
    QuizMvpPresenter<QuizMvpView> mPresenter;

    @BindView(R.id.end_quiz)
    View endQuiz;

    @BindView(R.id.coinTextView)
    TextView coinTV;

    @BindView(R.id.gemTextView)
    TextView gemTV;

    @BindView(R.id.rankTextView)
    TextView rankTV;

    @BindView(R.id.xpTextView)
    TextView xpTV;

    @BindView(R.id.quiz_type)
    TextView quizTypeTv;

    private static int type ;

    private static long id;

    private Timestamp timestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        Bundle bundle = getIntent().getExtras();

        type = bundle.getInt(AppConstants.QUIZ_TYPE);

        id = bundle.getLong(AppConstants.QUIZ_ID);

        switch (type) {
            case 0:
                showNormalQuiz();
                quizTypeTv.setText("آزمون عادی");
                break;
            case 1:
                showQuizTypeOne(id);
                quizTypeTv.setText("زمین بازی سه ستاره");
                break;
            case 2:
                showQuizTypeTwo(id);
                quizTypeTv.setText("زمین بازی فازی");
                break;
        }

//        Toast.makeText(this, "new quiz: type:"+type+", id:"+id, Toast.LENGTH_SHORT).show();

    }


    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        //TODO: implement backPress and handle fragments behaviour
        super.onBackPressed();
    }

    @Override
    public void showNormalQuiz() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.quiz_frag_holder, NormalQuizFragment.newInstance(), NormalQuizFragment.TAG)
                .commit();
    }

    @Override
    public void showQuizTypeOne(long quizId) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        FirstQuizFragment firstQuizFragment = new FirstQuizFragment();

        Bundle bundle = new Bundle();
        bundle.putLong("QUIZ_ID" , quizId);
        firstQuizFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.quiz_frag_holder,firstQuizFragment,FirstQuizFragment.TAG);
        fragmentTransaction.commit();

//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.quiz_frag_holder, FirstQuizFragment.newInstance(), FirstQuizFragment.TAG)
//                .commit();
    }

    @Override
    public void showQuizTypeTwo(long quizId) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        SecondQuizFragment secondQuizFragment = new SecondQuizFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("QUIZ_ID" , quizId);
        secondQuizFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.quiz_frag_holder,secondQuizFragment,SecondQuizFragment.TAG);
        fragmentTransaction.commit();

//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.quiz_frag_holder, SecondQuizFragment.newInstance(), SecondQuizFragment.TAG)
//                .commit();
    }

    public void refreshUserDetails(){
        mPresenter.refreshDetails();
    }

    @Override
    public void refreshUserDetailsView(UserDetails userDetails) {
        coinTV.setText(getResources().getString(R.string.coin) + ": " + userDetails.getCoin());
        gemTV.setText(getResources().getString(R.string.gem) + ": " + userDetails.getGems());
        rankTV.setText(getResources().getString(R.string.rank) + ": " + userDetails.getLevel());
        xpTV.setText(getResources().getString(R.string.xp) + ":" + userDetails.getXp());
    }

    @Override
    public void increaseXP(int deltaXP) {
        Toast.makeText(getApplicationContext(),"xp ziad shod : " + deltaXP,Toast.LENGTH_LONG).show();

    }

    @Override
    public void increaseCoin(int deltaCoin) {
        Toast.makeText(getApplicationContext(),"coin ziad shod : " + deltaCoin,Toast.LENGTH_LONG).show();
    }

    @Override
    public void increaseGem(int deltaGem) {
        Toast.makeText(getApplicationContext(),"gem ziad shod : " + deltaGem,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFragmentAttached() {
        super.onFragmentAttached();
        Log.d("size_frag", "size : "+getSupportFragmentManager().getFragments().size());
    }

    @Override
    public void onFragmentDetached(String tag) {
        super.onFragmentDetached(tag);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .remove(fragment)
                    .commitNow();
        }

    }

    @OnClick(R.id.end_quiz)
    void endQuiz(){
        //TODO: implement end quiz
        onBackPressed();
    }

    public void navigateToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
