package com.example.emad.unknown.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by emad on 4/1/17.
 */

public class SnackBarFactroy {

    public static Snackbar showSnakbar(View view, String massage){
        return Snackbar.make(view , massage, Snackbar.LENGTH_INDEFINITE);
    }
}
