package com.example.emad.unknown.data.manager.remote.sockets;

import android.util.Log;

import com.example.emad.unknown.data.model.UserAuth;
import com.example.emad.unknown.data.local.entities.UserDeltaDetails;
import com.example.emad.unknown.data.local.entities.UserDetails;
import com.example.emad.unknown.data.remote.api.API;
import com.example.emad.unknown.data.remote.retrofit.ServiceGenerator;
import com.example.emad.unknown.util.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by armin on 5/3/17.
 */

public class SyncDetailsSocket implements Observable.OnSubscribe<UserDetails> {

    private UserDeltaDetails userDeltaDetails;
    private UserAuth userAuth;

    public static Observable<UserDetails> getObservable(UserDeltaDetails userDeltaDetails, UserAuth userAuth){
        return  Observable.create(new SyncDetailsSocket(userDeltaDetails,userAuth));
    }

    private SyncDetailsSocket(UserDeltaDetails userDeltaDetails, UserAuth userAuth){
        this.userDeltaDetails = userDeltaDetails;
        this.userAuth = userAuth;
    }

    @Override
    public void call(final Subscriber<? super UserDetails> subscriber) {
        API api = ServiceGenerator.createService(API.class,userAuth);
        Call<UserDetails> syncDetails = api.syncDetails(userDeltaDetails);
        syncDetails.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                subscriber.onNext(response.body());
                subscriber.onCompleted();
            }

            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                Log.d(AppConstants.Log.RETROFIT,"error connecting to server...");
                subscriber.onError(t);

            }
        });

    }
}
